import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetsPlacedComponent } from './bets-placed.component';

describe('BetsPlacedComponent', () => {
  let component: BetsPlacedComponent;
  let fixture: ComponentFixture<BetsPlacedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetsPlacedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetsPlacedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
