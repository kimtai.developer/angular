import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { PagerService } from 'src/app/services/pager.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-bets-placed',
  templateUrl: './bets-placed.component.html',
  styleUrls: ['./bets-placed.component.css']
})
export class BetsPlacedComponent implements OnInit {
  subscription: Subscription;
  navigationSubscription;
  metaSubscription: Subscription;
  slips: any = [];

  // array of all items to be paged
  private allItems: number = 1;
  private path: string;
  private per_page: number;
  public load: boolean = false;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];

  constructor(
    private userservice: UserService,
    private location: Location,
    private titleService: Title,
    private router: Router,
    private pagerService: PagerService,
    public translate: TranslateService) {

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialiseInvites();
      }
    });

    this.setTitle('Rahisibet - Bets');

    this.subscription = this.userservice.getSlips().subscribe(data => {
      this.slips.length = 0;
      if (data.length != 0) {
        data.forEach(el => {
          this.slips.push(el)
        });
      }
      this.load = false;
    });

    this.metaSubscription = this.userservice.getMetadata().subscribe(data => {
      this.allItems = data.total;
      this.path = data.path;
      this.per_page = data.per_page;
      // initialize to page 1
      this.setPage(1);
    });
  }

  ngOnInit() { }

  public setTitle(newTitle: string) { this.titleService.setTitle(newTitle); }

  goBack(): void {
    this.location.back();
  }

  getSlipDetail(id) {
    this.userservice.getSingleSlip(id).subscribe(data => { });
  }

  initialiseInvites() {
    if (this.router.url == '/slip') {
      let id = JSON.parse(localStorage.getItem("slip"));
      this.load = true;
      this.getSlipDetail(id);
    }
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  setPage(page: number, flag?: number) {
    this.pager = this.pagerService.getPager(this.allItems, page, this.per_page);
    if (flag) {
      this.userservice.getNextSlipPage(page, this.path).subscribe(data => { });
    }
  }
}
