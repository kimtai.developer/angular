import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title }     from '@angular/platform-browser';
import { UserService } from 'src/app/services/user.service';
import { Invite } from 'src/app/models/invite.model';



@Component({
  selector: 'app-invite-a-friend',
  templateUrl: './invite-a-friend.component.html',
  styleUrls: ['./invite-a-friend.component.css']
})
export class InviteAFriendComponent implements OnInit {

  infoMainForm: FormGroup;

  submitted = false; //
  error: boolean = false;
  success: boolean = false;

  constructor(private formBuilder: FormBuilder, private userservice: UserService,private titleService: Title) {
    this.setTitle('Rahisibet - Invite A friend');
   }

  ngOnInit() {
    this.infoMainForm = this.formBuilder.group(
      {
        phone: [null, [Validators.required, Validators.pattern("^[0-8]*$"),Validators.minLength(8),Validators.maxLength(8)]]
      });
  
  }
  // convenience getter for easy access to form fields
  get f() { return this.infoMainForm.controls; }

  public setTitle( newTitle: string) {  this.titleService.setTitle( newTitle );  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.infoMainForm.invalid) {
      return;
    }

    let invite = new Invite();
    invite.friend = this.infoMainForm.value.phone;
    let tmpBet = Object.assign({}, invite);
    
    this.userservice.inviteFriend(tmpBet).subscribe(data => {
      
      if (data) {
        this.success = true;
        this.error = false;
      } else {
        this.success = false;
        this.error = true;
      }
    },(error) =>
    {
      this.error = true;
      this.success = false;
    })
  }

}
