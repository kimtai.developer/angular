import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteAFriendComponent } from './invite-a-friend.component';

describe('InviteAFriendComponent', () => {
  let component: InviteAFriendComponent;
  let fixture: ComponentFixture<InviteAFriendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviteAFriendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteAFriendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
