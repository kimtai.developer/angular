import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../../../helpers/must-match.validator';
import { Profile } from 'src/app/models/profile.model';
import { UserService } from 'src/app/services/user.service';
import { Password } from 'src/app/models/password.model';
import { Title }     from '@angular/platform-browser';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

    infoMainForm: FormGroup;
    passwordForm: FormGroup;

    submitted = false;
    passsubmitted = false;

    profile_error = false;
    profile_update = false;

    password_error = false;
    password_update = false;

    profile: any = [];
    zones: any = [];
    user_id: number;
    zone_name:string;
    zone_id: number ;

    constructor(private formBuilder: FormBuilder, private userservice: UserService,private titleService: Title) {

        this.setTitle('Rahisibet - Profile');

        if (localStorage.getItem("currentUser") !== null) {
            let user = JSON.parse(localStorage.getItem("currentUser"));
            this.user_id = user.id;            

            if (user.profile != null) {
                this.profile = user.profile;
                this.zone_name = user.profile.zone.name;
                this.zone_id = user.profile.zone.id;
            }
            else {
                this.zone_name = "";
                this.zone_id = 0;
                this.profile.name = "";
                this.profile.surname = "";
                this.profile.email = "";
                this.profile.gender = "";
                this.profile.town = "";

            }
        }
    }

    ngOnInit() {
        this.userservice.getZones().subscribe(data => { this.zones = data; });
        this.infoMainForm = this.formBuilder.group(
            {
                name: [this.profile.name, [Validators.required, Validators.pattern("^[A-Za-z]*$")]],
                surname: [this.profile.surname, [Validators.required, Validators.pattern("^[A-Za-z]*$")]],
                town: [this.profile.town, [Validators.pattern("^[A-Za-z]*$")]],
                email: [this.profile.email, [Validators.email]],
                gender: ['male'],
                zone: [this.zone_id, [Validators.required]]
            });
        this.passwordForm = this.formBuilder.group(
            {
                password: [null, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(6), Validators.maxLength(6)]],
                confirmPassword: [null, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(6), Validators.maxLength(6)]]
            }, {
                validator: MustMatch('password', 'confirmPassword')
            });
    }

    // convenience getter for easy access to form fields
    get f() { return this.infoMainForm.controls; }
    get passf() { return this.passwordForm.controls; }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.infoMainForm.invalid) {
            return;
        }
        let profile = new Profile();
        profile.name = this.infoMainForm.value.name;
        profile.email = this.infoMainForm.value.email;
        profile.surname = this.infoMainForm.value.surname;
        profile.gender = this.infoMainForm.value.gender;
        profile.town = this.infoMainForm.value.town;
        profile.zone = this.infoMainForm.value.zone;

        let tmpBet = Object.assign({}, profile);

        this.userservice.updateProfile(tmpBet).subscribe(data => {
            if (data.success) {
                this.userservice.getUser(this.user_id).subscribe(data => {
                    if (localStorage.getItem("currentUser") !== null) {
                        localStorage.setItem('currentUser', JSON.stringify(data));
                    }
                });
                this.profile_update = true;
                this.profile_error = false;
            } else {
                this.profile_error = true;
                this.profile_update = false;
            }
        });
    }

    public setTitle( newTitle: string) {  this.titleService.setTitle( newTitle );  }

    onPasswordSubmit() {
        this.passsubmitted = true;
        // stop here if form is invalid
        if (this.passwordForm.invalid) {
            return;
        }

        let password_obj = new Password();
        password_obj.password = this.passwordForm.value.password;
        password_obj.confirmPassword = this.passwordForm.value.confirmPassword;

        let tmpBet = Object.assign({}, password_obj);

        this.userservice.updatePassword(tmpBet).subscribe(data => {
            if (data.success) {
                this.password_update = true;
                this.password_error = false;
            } else {
                this.password_error = true;
                this.password_update = false;
            }
        });
    }
}
