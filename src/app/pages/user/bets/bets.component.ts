import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { PagerService } from 'src/app/services/pager.service';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'app-bets',
  templateUrl: './bets.component.html',
  styleUrls: ['./bets.component.css']
})
export class BetsComponent implements OnInit {

  betslip: any = [];
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  subscription: Subscription;
  metaSubscription: Subscription;
  flag: any;

  // array of all items to be paged
  private allItems: number = 1;
  private path: string;
  private per_page: number;
  public load: boolean = false;

  //date picker
  public mytime: Date = new Date();
  currentYear: any = this.mytime.getUTCFullYear();
  currentDate: any = this.mytime.getUTCDate() - 1;
  currentMonth: any = this.mytime.getUTCMonth() + 1; //months from 1-12

  public myDatePickerOptions: IMyDpOptions = {
    showInputField: true,
    height: '34px',
    width: '160px',
    dateFormat: 'yyyy-mm-dd',
    inline: false,
  };

  public myDatePickerOptions1: IMyDpOptions = {
    showInputField: true,
    height: '34px',
    width: '45%',
    dateFormat: 'yyyy-mm-dd',
    inline: false,
  };

  public myForm: FormGroup;
  startdate: string = '';
  enddate: string = '';
  bydate: boolean = false;
  public img = this.pagerService.getLogo();
  //end

  constructor(
    private userservice: UserService,
    private pagerService: PagerService,
    private titleService: Title,
    private router: Router,
    private formBuilder: FormBuilder,
    public translate: TranslateService) {

    this.setTitle('Rahisibet - Betslips');

    this.subscription = this.userservice.getSlips().subscribe(data => {
      this.flag = data.length;
      this.betslip.length = 0;
      if (data.length != 0) {
        data.forEach(el => {
          this.betslip.push(el)
        });
      }
      this.load = false;
    });

    this.metaSubscription = this.userservice.getMetadata().subscribe(data => {
      this.allItems = data.total;
      this.path = data.path;
      this.per_page = data.per_page;
      // initialize to page 1
      this.setPage(1);
    });

    translate.getLangs();
  }

  ngOnInit() {
    this.load = true;
    this.userservice.getBetslip().subscribe(data => { });

    // datepicker
    this.myForm = this.formBuilder.group({
      fromDate: [null, Validators.required],
      toDate: [null, Validators.required]
    });
  }

  public setTitle(newTitle: string) { this.titleService.setTitle(newTitle); }

  toggleAccordian(event) {
    var element = event.target;
    element.classList.toggle("active");

    var panel = element.nextSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }

  setPage(page: number, flag?: number) {
    this.pager = this.pagerService.getPager(this.allItems, page, this.per_page);
    if (flag) {
      if (!this.bydate) {
        this.userservice.getNextPage(page, this.path).subscribe(data => { });
      }
      else if (this.bydate) {
        this.userservice.getNextByDateRange(page, this.path, this.startdate, this.enddate).subscribe(data => { });
      }
    }
  }

  getSlipById(id) {
    localStorage.setItem('slip', JSON.stringify(id));
    if (localStorage.getItem("slip") != null) {
      this.router.navigate(['slip']);
    }
  }

  // datepicker  set today date using the patchValue function.
  fromDateChanged(event: IMyDateModel) {
    if (event.formatted) {
      this.startdate = event.formatted;
    }
  }

  toDateChanged(event: IMyDateModel) {
    if (event.formatted) {
      this.enddate = event.formatted;
    }
  }

  getSlipsByDate() {

    this.flag = false;
    this.load = true;

    if (this.startdate == '' || this.enddate == '') {
      this.userservice.getBetslip().subscribe(data => { });
    } else {
      this.bydate = true;
      this.userservice.getSlipsByDateRange(this.startdate, this.enddate).subscribe(data => { });
    }

  }
  // end datepicker

  generatePDF(slip) {
    var doc = new jsPDF('p', 'pt', 'a4');

    if (this.translate.currentLang == 'en') {
      var col = ["#", "Event " + slip.bet_code, "Odds", "Startdate", "Pick", "Status", "Result", "Scores"];
    } else {
      var col = ["#", "Evénement " + slip.bet_code, "Cote", "Date début", "Choisir", "Statut", "Résultat", "Les scores"];
    }

    var rows = [];
    let x = 1;

    slip.matches.forEach(el => {
      let tmp = [];
      tmp[0] = x;
      tmp[1] = el.name;
      tmp[2] = el.odd;
      tmp[3] = el.scheduled;
      tmp[4] = el.pick;

      if (el.status == 1 && el.resulted == 1 && el.won == 0) {
        if (this.translate.currentLang == 'en') {
          tmp[5] = 'Finished';
          tmp[6] = 'Lost';
        } else {
          tmp[5] = 'Terminé';
          tmp[6] = 'Perdu';
        }
      } else if (el.status == 0 && el.resulted == 0 && el.won == 0) {
        if (this.translate.currentLang == 'en') {
          tmp[5] = 'Running';
          tmp[6] = 'Running'
        } else {
          tmp[5] = 'Fonctionnement';
          tmp[6] = 'Fonctionnement';
        }
      } else if (el.status == 1 && el.resulted == 1 && el.won == 1) {
        if (this.translate.currentLang == 'en') {
          tmp[5] = 'Finnished';
          tmp[6] = 'Won'
        } else {
          tmp[5] = 'Terminé';
          tmp[6] = 'Gagné';
        }
      }

      if (el.scores != null) {
        tmp[7] = el.scores.ft;
      }

      x++;
      rows.push(tmp);

    });

    var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
    var base64Img = this.pagerService.getLogo();
    var pageContent = function (data) {
      // HEADER
      if (base64Img) {
        doc.addImage(base64Img, 'PNG', (pageWidth / 2) - 50, 10, 160, 30);
      }      
    };

    for (let index = 0; index < 3; index++) {
      let xtr = [];

      if (index == 1) {

        xtr[0] = index;
        if (this.translate.currentLang == 'en') {
          xtr[1] = "Amount";
        } else {
          xtr[1] = "Montants";
        }
        xtr[2] = "";
        xtr[3] = "";
        xtr[4] = "";
        xtr[5] = "";
        xtr[6] = "BIF " + slip.stake;

      } else if (index == 2) {

        xtr[0] = index;
        if (this.translate.currentLang == 'en') {
          xtr[1] = "Possible Win";
        } else {
          xtr[1] = "Victoire possible";
        }
        xtr[2] = "";
        xtr[3] = "";
        xtr[4] = "";
        xtr[5] = "";
        xtr[6] = "BIF " + slip.possible_win;

      } else {

        xtr[0] = "";
        xtr[1] = "";
        xtr[2] = "";
        xtr[3] = "";
        xtr[4] = "";
        xtr[5] = "";
        xtr[6] = "";

      }

      rows.push(xtr);
    }

    doc.autoTable(col,rows,{
      // head: [col],
      // body: rows,
      theme: 'grid',
      styles: { fontSize: 6.5, margin: 10 },
      headStyles: { fillColor: [7, 59, 107], textColor: [255, 255, 255], halign: 'center', valign: 'middle' },
      bodyStyles: { halign: 'center' },
      margin: {top: 50},
      didDrawPage : pageContent
    });

    if (this.translate.currentLang == 'en') {
      doc.save('betslip.pdf');
    } else {
      doc.save("Coupon de pari.pdf");
    }
  }

  printComponent(id) {
    let printContents, popupWin;
    printContents = document.getElementById(id).innerHTML;
    // printContents = '';
    popupWin = window.open('', '_blank', 'top=0,left=0,width=300');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
            td,
            th,
            tr,
            table {
                border-top: 0px solid black;
                border-collapse: collapse;
                font-family:'Roboto Condensed','segoe ui';
                font-size:10px;
            }
            p{
              font-family:'Roboto Condensed','segoe ui';
              font-size:10px;
              font-weight:bold;
            }
            th {
              border-top: 1px solid black;
              border-bottom: 1px solid black;
              padding: 5px 0px;
              text-align:center;
            }
            td {
              word-break: break-all;
              text-align:center;
            }
            td.event {
              text-align: left;
            }
            tr.border-top {
              border-top: 1px solid black;
            }
            .centered {
                text-align: center;
                align-content: center;
            }

            .ticket {
                max-width: 300px;
            }

            img {
                max-width: 150px;
                width: inherit;
            }

            @media print {
                .hidden-print,
                .hidden-print * {
                    display: none !important;
                }
            }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
