import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutobetComponent } from './autobet.component';

describe('AutobetComponent', () => {
  let component: AutobetComponent;
  let fixture: ComponentFixture<AutobetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutobetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutobetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
