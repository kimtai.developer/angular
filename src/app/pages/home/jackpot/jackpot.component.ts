import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Market } from 'src/app/models/market.model';
import { JackpotService } from 'src/app/services/jackpot.services';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatashareService } from 'src/app/services/datashare.service';

@Component({
  selector: 'app-jackpot',
  templateUrl: './jackpot.component.html',
  styleUrls: ['./jackpot.component.css']
})
export class JackpotComponent implements OnInit {
  subscription: Subscription
  autobetSubscription: Subscription;
  betslipSubscription: Subscription;
  slides = new Subscription;

  oddsfeed: any = [];
  betslip: any = [];
  en_slides: any = [];
  fr_slides: any = [];
  count: number;
  count_random: any = [];
  markets = new Market();
  public load: boolean = false;

  constructor(
    private jackpot: JackpotService,
    private titleService: Title,
    private router: Router,
    public translate: TranslateService,
    private datashare: DatashareService
  ) {
    this.setTitle('Rahisibet - Jackpot');
    this.subscription = this.jackpot.getAutobetGames().subscribe(data => {

      this.oddsfeed.length = 0;
      data.forEach(el => {
        this.oddsfeed.push(el);
      });

      if (localStorage.getItem("jackpot-betslip") !== null) {
        let localbetslip = JSON.parse(localStorage.getItem("jackpot-betslip"));
        this.oddsfeed.forEach(el => {
          let selectedMatch = localbetslip.find(x => x.match_id == el.match_id);
          if (selectedMatch) {
            el.selected_outcome = selectedMatch.selected_outcome;
            el.selected_market = selectedMatch.selected_market;
          } else {
            el.selected_outcome = "";
            el.selected_outcome = "";
          }
        });
      }

      this.count = this.oddsfeed.length;

      this.oddsfeed.forEach(el => {
        this.markets.sport = el.sport;
        this.markets.sport_id = el.sport_id;
        this.markets.market.length = 0;
        for (var index in el.markets) {
          if (!this.markets.market.includes(index)) {
            this.markets.market.push(index);
          }
        }
      });

      this.markets.market.reverse();
      this.load = false;
    });

    this.autobetSubscription = this.jackpot.getAutobetSelect().subscribe(data => {
      if (data != null && data.length > 0) {

        this.oddsfeed.forEach(el => {
          let selectedMatch = data.find(x => x.match_id == el.match_id);

          if (selectedMatch) {
            el.selected_outcome = selectedMatch.selected_outcome;
            el.selected_market = selectedMatch.selected_market;
          } else {
            el.selected_outcome = "";
            el.selected_outcome = "";
          }
        });
      }
      else {
        this.oddsfeed.forEach(el => {
          el.selected_outcome = "";
          el.selected_outcome = "";
        });
      }
    });

    this.slides = this.datashare.getSlidesSubject().subscribe(data => {
      this.fr_slides.length = 0;
      this.en_slides.length = 0;
      data.forEach(element => {
        if (element.language == 'fr') {
          this.fr_slides.push(element)
        } else {
          this.en_slides.push(element)
        }
      });
    })
  }

  ngOnInit() {
    this.markets.markettypes['1x2'] = 3;
    this.markets.markettypes['Double chance'] = 0;

    this.jackpot.setAutobetSelect();
    this.load = true;
    this.datashare.getSlides().subscribe();

    let url = this.router.url;
    localStorage.setItem('reload-url', JSON.stringify(url));
  }

  public setTitle(newTitle: string) { this.titleService.setTitle(newTitle); }

  addToBetslip(index, outcome, match_id, market) {

    let betslip = this.oddsfeed.find(x => x.match_id == match_id);
    betslip.selected_outcome = outcome;
    betslip.selected_market = market;

    // add bet to slip
    this.jackpot.addBetslip(betslip);
  }

  public generatePDF() {
    var doc = new jsPDF('p', 'pt', 'a4');

    if (this.translate.currentLang == 'en') {
      var col = ["Id", "Jackpot Games", "Sport", "Country", "League", "Game ID", "Schedule", "1", "X", "2"];
    } else {
      var col = ["Id", "les jeux de Jackpot", "Sport", "Pays", "Ligue", "ID du jeu", "Programmer", "1", "X", "2"];
    }

    var rows = [];
    let x = 1;

    this.oddsfeed.forEach(el => {

      let tmp = [];
      tmp[0] = x;

      if (this.translate.currentLang == 'en') {
        tmp[1] = el.name;
        tmp[2] = el.sport;
        tmp[3] = el.country;
        tmp[4] = el.tournament
      } else {
        tmp[1] = el.french;
        tmp[2] = el.sport_fr_name;
        tmp[3] = el.country_fr_name;
        tmp[4] = el.tournament_fr_name;
      }

      tmp[5] = el.game_id;
      tmp[6] = el.scheduled;

      let y = 7;
      el.markets['1x2'].forEach(elem => {
        tmp[y] = elem.odds;
        y++;
      });

      x++;
      rows.push(tmp);
    });

    for (let index = 0; index < 3; index++) {
      let xtr = [];

      if (index == 1) {

        xtr[0] = index;
        if (this.translate.currentLang == 'en') {
          xtr[1] = "Amount";
        } else {
          xtr[1] = "Montants";
        }
        xtr[2] = "";
        xtr[3] = "";
        xtr[4] = "";
        xtr[5] = "";
        xtr[6] = "BIF 2,000";
        xtr[7] = "";
        xtr[8] = "";
        xtr[9] = "";
      } else if (index == 2) {
        xtr[0] = index;
        if (this.translate.currentLang == 'en') {
          xtr[1] = "Possible Win";
        } else {
          xtr[1] = "Victoire possible";
        }
        xtr[2] = "";
        xtr[3] = "";
        xtr[4] = "";
        xtr[5] = "";
        xtr[6] = "BIF 5,000,000";
        xtr[7] = "";
        xtr[8] = "";
        xtr[9] = "";
      } else {

        xtr[0] = "";
        xtr[1] = "";
        xtr[2] = "";
        xtr[3] = "";
        xtr[4] = "";
        xtr[5] = "";
        xtr[6] = "";
        xtr[7] = "";
        xtr[8] = "";
        xtr[9] = "";

      }

      rows.push(xtr);
    }

    var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
    var base64Img = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAAAyCAYAAAA3FLVzAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAACHDwAAjA8AAP1SAACBQAAAfXkAAOmLAAA85QAAGcxzPIV3AAAKOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAEjHnZZ3VFTXFofPvXd6oc0wAlKG3rvAANJ7k15FYZgZYCgDDjM0sSGiAhFFRJoiSFDEgNFQJFZEsRAUVLAHJAgoMRhFVCxvRtaLrqy89/Ly++Osb+2z97n77L3PWhcAkqcvl5cGSwGQyhPwgzyc6RGRUXTsAIABHmCAKQBMVka6X7B7CBDJy82FniFyAl8EAfB6WLwCcNPQM4BOB/+fpFnpfIHomAARm7M5GSwRF4g4JUuQLrbPipgalyxmGCVmvihBEcuJOWGRDT77LLKjmNmpPLaIxTmns1PZYu4V8bZMIUfEiK+ICzO5nCwR3xKxRoowlSviN+LYVA4zAwAUSWwXcFiJIjYRMYkfEuQi4uUA4EgJX3HcVyzgZAvEl3JJS8/hcxMSBXQdli7d1NqaQffkZKVwBALDACYrmcln013SUtOZvBwAFu/8WTLi2tJFRbY0tba0NDQzMv2qUP91829K3NtFehn4uWcQrf+L7a/80hoAYMyJarPziy2uCoDOLQDI3fti0zgAgKSobx3Xv7oPTTwviQJBuo2xcVZWlhGXwzISF/QP/U+Hv6GvvmckPu6P8tBdOfFMYYqALq4bKy0lTcinZ6QzWRy64Z+H+B8H/nUeBkGceA6fwxNFhImmjMtLELWbx+YKuGk8Opf3n5r4D8P+pMW5FonS+BFQY4yA1HUqQH7tBygKESDR+8Vd/6NvvvgwIH554SqTi3P/7zf9Z8Gl4iWDm/A5ziUohM4S8jMX98TPEqABAUgCKpAHykAd6ABDYAasgC1wBG7AG/iDEBAJVgMWSASpgA+yQB7YBApBMdgJ9oBqUAcaQTNoBcdBJzgFzoNL4Bq4AW6D+2AUTIBnYBa8BgsQBGEhMkSB5CEVSBPSh8wgBmQPuUG+UBAUCcVCCRAPEkJ50GaoGCqDqqF6qBn6HjoJnYeuQIPQXWgMmoZ+h97BCEyCqbASrAUbwwzYCfaBQ+BVcAK8Bs6FC+AdcCXcAB+FO+Dz8DX4NjwKP4PnEIAQERqiihgiDMQF8UeikHiEj6xHipAKpAFpRbqRPuQmMorMIG9RGBQFRUcZomxRnqhQFAu1BrUeVYKqRh1GdaB6UTdRY6hZ1Ec0Ga2I1kfboL3QEegEdBa6EF2BbkK3oy+ib6Mn0K8xGAwNo42xwnhiIjFJmLWYEsw+TBvmHGYQM46Zw2Kx8lh9rB3WH8vECrCF2CrsUexZ7BB2AvsGR8Sp4Mxw7rgoHA+Xj6vAHcGdwQ3hJnELeCm8Jt4G749n43PwpfhGfDf+On4Cv0CQJmgT7AghhCTCJkIloZVwkfCA8JJIJKoRrYmBRC5xI7GSeIx4mThGfEuSIemRXEjRJCFpB+kQ6RzpLuklmUzWIjuSo8gC8g5yM/kC+RH5jQRFwkjCS4ItsUGiRqJDYkjiuSReUlPSSXK1ZK5kheQJyeuSM1J4KS0pFymm1HqpGqmTUiNSc9IUaVNpf+lU6RLpI9JXpKdksDJaMm4ybJkCmYMyF2TGKQhFneJCYVE2UxopFykTVAxVm+pFTaIWU7+jDlBnZWVkl8mGyWbL1sielh2lITQtmhcthVZKO04bpr1borTEaQlnyfYlrUuGlszLLZVzlOPIFcm1yd2WeydPl3eTT5bfJd8p/1ABpaCnEKiQpbBf4aLCzFLqUtulrKVFS48vvacIK+opBimuVTyo2K84p6Ss5KGUrlSldEFpRpmm7KicpFyufEZ5WoWiYq/CVSlXOavylC5Ld6Kn0CvpvfRZVUVVT1Whar3qgOqCmrZaqFq+WpvaQ3WCOkM9Xr1cvUd9VkNFw08jT6NF454mXpOhmai5V7NPc15LWytca6tWp9aUtpy2l3audov2Ax2yjoPOGp0GnVu6GF2GbrLuPt0berCehV6iXo3edX1Y31Kfq79Pf9AAbWBtwDNoMBgxJBk6GWYathiOGdGMfI3yjTqNnhtrGEcZ7zLuM/5oYmGSYtJoct9UxtTbNN+02/R3Mz0zllmN2S1zsrm7+QbzLvMXy/SXcZbtX3bHgmLhZ7HVosfig6WVJd+y1XLaSsMq1qrWaoRBZQQwShiXrdHWztYbrE9Zv7WxtBHYHLf5zdbQNtn2iO3Ucu3lnOWNy8ft1OyYdvV2o/Z0+1j7A/ajDqoOTIcGh8eO6o5sxybHSSddpySno07PnU2c+c7tzvMuNi7rXM65Iq4erkWuA24ybqFu1W6P3NXcE9xb3Gc9LDzWepzzRHv6eO7yHPFS8mJ5NXvNelt5r/Pu9SH5BPtU+zz21fPl+3b7wX7efrv9HqzQXMFb0ekP/L38d/s/DNAOWBPwYyAmMCCwJvBJkGlQXlBfMCU4JvhI8OsQ55DSkPuhOqHC0J4wybDosOaw+XDX8LLw0QjjiHUR1yIVIrmRXVHYqLCopqi5lW4r96yciLaILoweXqW9KnvVldUKq1NWn46RjGHGnIhFx4bHHol9z/RnNjDn4rziauNmWS6svaxnbEd2OXuaY8cp40zG28WXxU8l2CXsTphOdEisSJzhunCruS+SPJPqkuaT/ZMPJX9KCU9pS8Wlxqae5Mnwknm9acpp2WmD6frphemja2zW7Fkzy/fhN2VAGasyugRU0c9Uv1BHuEU4lmmfWZP5Jiss60S2dDYvuz9HL2d7zmSue+63a1FrWWt78lTzNuWNrXNaV78eWh+3vmeD+oaCDRMbPTYe3kTYlLzpp3yT/LL8V5vDN3cXKBVsLBjf4rGlpVCikF84stV2a9021DbutoHt5turtn8sYhddLTYprih+X8IqufqN6TeV33zaEb9joNSydP9OzE7ezuFdDrsOl0mX5ZaN7/bb3VFOLy8qf7UnZs+VimUVdXsJe4V7Ryt9K7uqNKp2Vr2vTqy+XeNc01arWLu9dn4fe9/Qfsf9rXVKdcV17w5wD9yp96jvaNBqqDiIOZh58EljWGPft4xvm5sUmoqbPhziHRo9HHS4t9mqufmI4pHSFrhF2DJ9NProje9cv+tqNWytb6O1FR8Dx4THnn4f+/3wcZ/jPScYJ1p/0Pyhtp3SXtQBdeR0zHYmdo52RXYNnvQ+2dNt293+o9GPh06pnqo5LXu69AzhTMGZT2dzz86dSz83cz7h/HhPTM/9CxEXbvUG9g5c9Ll4+ZL7pQt9Tn1nL9tdPnXF5srJq4yrndcsr3X0W/S3/2TxU/uA5UDHdavrXTesb3QPLh88M+QwdP6m681Lt7xuXbu94vbgcOjwnZHokdE77DtTd1PuvriXeW/h/sYH6AdFD6UeVjxSfNTws+7PbaOWo6fHXMf6Hwc/vj/OGn/2S8Yv7ycKnpCfVEyqTDZPmU2dmnafvvF05dOJZ+nPFmYKf5X+tfa5zvMffnP8rX82YnbiBf/Fp99LXsq/PPRq2aueuYC5R69TXy/MF72Rf3P4LeNt37vwd5MLWe+x7ys/6H7o/ujz8cGn1E+f/gUDmPP8usTo0wAAAAlwSFlzAAAOxAAADsQBlSsOGwAAHxxJREFUeF7tnQdgU9Uax/9ZTZqOpC0to0BZshVkIyLKUOCBDxmCD0WEpwgIooIDUYY4EN/DgcpGhgKyUWTIkCkiILJkCAXa0t0mHWn2fd93c0PTku7xQPPTS3LPuSP33POdb5xRmUDAhw8flYJc+vThw0clUCkaLj41C06rHaBbqbRqROi1Uo4PH38vyl3gLsWmYcEPv2PXyes4eSrGlShjRUq3kbl24aDvKjk63VsXPVpH4YW+LRCmC5Ayffj461JuAjdt2QFMX/ULYLICoQHw91NCIZfB7nDC5hRIubluI5fJSNbkUCg4T4DZYgNSshBUPRizh3fGqL73isf58PFXpMwCN2PpAUydtweI0CEoUA2b3QFzpoWEKAPqyFC0bhCB9jVDoFTK6WYymGx2HLqWglPRKXDEG4BqOviTmcnCmWU0AdlWLH69D0b0vke6gw8ffx1KLXBXEwyoO3Q+oFFBp9MiI8cKIS4ND3VuiOdJSz3+YBPpyMJZTObnl9+fxPHj16AkwdSqlchIzkS4PgCXlo6ALtBfOtKHjzufUgnczOUH8dbcXQiqXxVWuxMW0lhjhrTD5y/3ko4oOVarHU/O+h5rt/4O/9rhkMkEmK6mYtPHT+CfHe+Sjvp7coMaoM2/XMb+369j9qiHULNKkJTj406jxAL3zzfXYcuRywghUzCdTMLmDari9IJnpNyyk0pmZbORi5FoskEXooXxSjI+Gd8d45/oKB3x9yAuKQP3jluB5FgyuxWUEKgByB+OXvk86oT7BO5OpUT9cPdP+AZbTlxzCdulJHz+QvdyFTYmjMzThHXj8Gz3pjDGpEFfLxwvfr4bHy0/JB3x98BgsiA5IwfBtUOhI184OJhMa7UK5OresWRmk2//N6fYAvfcrB9w6HwcQkICkH4xHme+HY0x/dtIueXPgpd7YsO0fjBcT4Euqgomzd+L5dtPSbllI4Mqs8GQjevkh564lIC9p2Ow60Q0dpPJdiU2HWaLXTry/wdHcyGn11O2mFah6B77FB1HfyXtVSw/nbqO4DZTsXbvH1LKrfCTCk4nnGYDnFmpcGYnw5kRR9+TIJjTXQfd4RTLpNx55E88MnENQqLCkH4hHuc3vohGtUKl3Ipl68GL6DN5LXS1wmC8nITTq0ejeZ1wKbdkNH9+Kc6evA6olAC9WCioQvPGFZtVBxcFd9A76ZPSurSth4mD2qJPh/rSFSqPP8gvbjpmOYKDNOTPysRulQyTFdeWjETtcvDh6oxYiOup2RDIB3+oSXXs+egJKaf8OUwNdaeRSxFYOwxZcenY/MEgPHpfrl+es7oXnOe3A/RaQK+FlTi/Cm5zGLGG8nfaZGp/yBv0gbLVGCjrPCjm30kUS+BkradB36gaDIkGXF8+CrUiQ6ScymHGskOY+vVhBIYGwGbMgXnbK1JOyeg6+VscvJCIALVSbE35Hdq4L5CEjItBoZRDQ8KooAyWObPNAWtqJsKr6hC34jmoWFAriYoSOIH+a/bsUvyRYISO/ULCSH7zA80isW/2EHG/PNlLDVzX8SsQRGYxm1MkT8i8YcCq6f0wRIpkmzeSsF/fTQLHEWm3lDkg2HJol6VMDplSTYms8SndboFA2k+mjYBm9AXINHrXOXcARQpc+zHLcDIpE2qqjP5+CrQiTcO+hbtcygTdmSt3g6p6vDSoDdo0rC5l3MqDL3+DA9EpcJIQjCX/bu6ER6Sc4sMCd+B8AgmcSty3OZyI0vvjUdJkFvqelpqF5aRR+dl0HiNfckggrSTowq5JtFceD140FSVwOWYrQgd/AQdpdq1UDpl03SoaJa5SY+pPjVF58tX203jmnU3Q164iNnKMMd6AiUM7YvazLg3FAieQwMlEgSMcNggkTIpG/cjiyIKQmQTH5e/oewbkwbXoAOlKThscafHQTkoh4Qtzpd3mFCpwx87fQFtqDXU1Xeajkw7lboDyrHJ8czupE8eNdDzZrxVWvN7HleEFWfdZCAoPRiZVxvQdk6CnylgS8gtcVo4Vw7o2wZLxD4v7bt5efhDvfP0zdKGBUgpVEvL7+t1dCxtnDpBSKpaKNillfedAo1HBRu8zjD4TV42WcsqflXvO4am3NkBPvriBNOubw+/HzGGdpNxbBU6wZkNe92Go+30j7ruxn10D68YhkIfUllIIB2k7RRD8x16SEm5vWMsXyOD3voe2BpmPLGG0ycnP0ZCWU5fbpiQTToFAalX19SKwctNx7D5x1XVzL8wd1wOZZP4oqukw8r9k85cDFvbZ8jFj2P0Y0OkuseV3E+zvh03Hr8L4F4m02Te9CHN6NgKoBlSksDFPdm2KBVMeheHkNUx8okMeYSsQZ27Zu1E2Gwxl57chWAxSCqEgUzPrGhwp56WE25sCBe7IHzdwZfcZmGJSYYxOrpjtUoLoPzDcgivJzv9q60lx3xtj/9kKfgqF6Gdt2HVOSi0r3vX1KwPbwukRrZRxUCXLjD9j0qSUOxsFmZSp68chhszIyuDZXvfg8NoXyIzsIqWUDmWX6XBmZNC33PcmyFVwxh2R9ioPts6cmfGunWJSoEnJ/o2KI3gVzNJtv2PExzsRTOZbtsWG4a3qYBG1hgXx4Zpf8Nryw/S0Trz/9P14fXB7KadovJmUg+9viK8n3jpCJpX81BpPzYeaNJsYoieMadmYP+FhPEeVp6KpaJPydsKrSRn1INT914n7+cn+TzApNj3VXtd7EXJSoez6CVStRor7lYE9ehesy3rAaQS0065CrouScgqnQImqDGFjnunVAv06NoCJZw1Qq8WVqzDG92tNzYoDaq0am/ZfkFLLHz0Jmlouv+noi9BPs9gc0s7/iwJd7r8F4tPn0xEceZX5V16k0ryqF6wrekAeEUUbyYmT627xqBypKgKVUiG24MVBQ/7eXZJf+cu5G1Jq+ZNjs4vBnDziT2l1I0quXa4lGLFizzl8sf5Xca7guespUk7x4dLhaYVyL69s3+kYfLruqBiccHL/YgXCz7Jw+yl8uf4o5tOzbCe/NiElU8qtePh9yExZ9I/7zdA7slmguKvgYFt+HGmXYDu9HLZfPoftzEo4TcV7H87USzDNJGsj8VfIw0ijUZ0lFUGaOTe4VhTF6oeraAbO3IJNx6PFGvVM6zpY+GZfKcc7s789ildXHALI7DsyfzjaN64h5RROSUzKU+Rjthi3AsGBLpOOMd4wIGbtGNSM0In7hfHZxmNYsP0MzpyLc0lLIDn31LCIHe4ZZvoux7bZg9GzbT3XCfnIb1I6SfgtJPCJK0ZBH6DGqStJeOnLPdhz6E9qhZQAaWRqIQBDNgb0vAfrpj8mXelW+k1eh817z9J5dA5jtqN1h3o4Nmeoa98LK3aewbDZWwET+bU6DUfQ6Fnoweg3iXMgyUJpc19D/Ppp3mvkUBlru89yDTZwy0imGdvmP4Oe9K6ZkpiUjj+3wbq2L2TBkeK+YCVhr9YB/k/8IO4XBPta1u+ehvPSj6KakWm09EkNPWknIZveh78a6t5LoLz7X9IZebHufBn2w3MgD+duCY9mmIrAkZ1AZS8FeTiLikRRqy00zxx1pXngVcMto5Zr+Cc7MGjqBinl9qJRrTB6SnrSIH/sOlZwVLMsnCWB43u4hc1GghIZGVKosP1MwtVt4mrIOs3E+EX7cYFafnVVHfyr6xGs10JHwsNTmXS1QxFYTYdeL32D2CQOABQNy2wVEjSr2YqWo5ehxdOLcOByMoLoWnq6li7YH7rQAOjqR2D90Svo/vIq14leCCWBkVXTQydtiuo6hNPvKoh/Td+IYe9+h6CwIKjpWG445NRgqLV+CCZ/MqROFXFe48COtzYechJMbZVg8Xnd90NEsNinWyhyqTHwQLBkwvLdM5AFcX8taRq7mYQlHZoh1BAUgu3wbJhn1YCQcgryKlGQBehJq5lEYZVzf194FBQBEbBuGQrzmt7SWXmx7pnjatg8hY2hXbk6CHJNIP1kf1duDqVFdBCz8+NV4GZvPo5lu85hHWmS25H2d0XQv4L40i8kktdaJrgq38r4+T9BG8SjG1yYkjPxxuB20p53Nv/8J/bQxtoL1LLr/JS4r244WkWFICPdRH6qFPWkWypYkEkI311dvOgad8lkWh2oOmQefo9JpxctwGaxkADaxL4tB2sbhj50dN3dJ69hPw9jKwjPx/ZeBCJvLt6HVfsuQh+pJ4vAhh6Nq+HtIR0wvmdztKeGL4MalfRrqaLWauplyF2+6indKn9qXmQKPzLf/oB1xwuwfj8C5g2Pw/RJJHJmB0OuclknDsM1KudA+L9hudkoesP+6zzYdr0KeQ0SNIUaQtYNKNpPgnayCerhv8EhRhl5PJkMcn0UnBwM2f2a62QPAj8Q4DdwMwk4PasH3Agomj4F5UMfQvnIQmhGHIN2ph1+//hUOiIvXgUunkweLZkbsoZVpZSKRVPCAE1oSABZn3Jxs6VnS6mlw9/P9QLdnLyUgOqD5yLdaodK4WqFjWQy3UWtOHdLFMZ4yh/Z715c+Pp5CHteR/Kq0dhDZuPBj/4FYcdE1A7VwmLPDbqoSCAvJhZPw4lViv8hE25o5wZI3/oyhK0TYd70ImLXjIHZZBEHJtyEtOkXO05LO6UjKT0L7608DF3VYPrdTtQPD8J3Mwdi+r86YM7z3bBvDpmCP76K2I3jMHZQW9QlLVYuKFTkpyXDcf5bOC5vhRC7n6w/Egipw9tJ1ZYrtv/Yy6JwFoTTkkVm5Oib/pZgI1O1yWD4dZoMmcqfNHtL+I+JhjOZ197hwqVGnLSnff+HZGrmdgm5UTYg7efkfliPcrZmQNFmHFStRkN1z1DIq7Wm31Sw9vZa0w3ZVrqkgGpBlbO61rbfrkItjlPkuxYNR1DltHERGcoQNdSQsP342zX0euNbNHpuiTiS5d5RS5FBVw7UqMTfYkzLQg2tCheX/Nt1UiHUIPNq0aR/oKE0Mic/r/RtCbNHxzk73FZH8YIc7Gpnkq9xmYR45Wt9oCdz2k1keDD6tq8vRXpdqBRKxCeVTfsv2Ul+ntQgcVnf1KL54PvPfaEHmpM2Ly/Yj3OmJsOZlgTBmAieQSA46PnIz5fDAcuiNsj5ohEciQXPILFtHwtZKA/5cv1uHpupqP8P8bsbOfmC8qj2dG2P96IPIZ9tvLRXFFQy1izpe9EUqlq4kEvCQfJhzsWk4nwxtotxaeT4J6MbVfY0i0PqhpDBaS9+iLWsKKnVTCPttfdSEmLTc0R/RFdVLy5yxNoim7TGm2Q+xa0aI51RNuQClSj7nh4UYg3dgowqm18BYx1b1guHg8rRDXk43Lst7ZWOK2RGQ7ofv5/oZCMGz9ws7lckgs0Eef2e0M4QoJ3qhGb8Vfj1mEu+VzU4jWQmy5SQh5KJ6MiC5csW5F+9Lp2ZC5ey4+KGm4EYRiY4yLS81S2Q1+1BB3sInCoQjuMLpb3yxavA6QJYTcuQQHZ5SfCj2tOs00w0eWoBmjw5v9Ct0RPz0OLfi3HgYiJ05HwzDqr81aKKbiW5U95JGxeqXlW2SmWy2mAh08mURc/qUft5pkD/1nUx85nOUkrZCfTPa76WFH5ejlZ6pYDksvBw85piJNgNry+z9thVyDq/iwHTNomjkSoENo2dNrHB5xE+3KmsbDEcmpEn4Nd/HWk98t8YOQleeBQcP8+C7dQKV5obM2n3DNI8cql+8DXpf3nIrYEdRaP+gMXDtKd6IJM74GQ/sZzxKnB1QgNY3uCMLdkwpnZNamDbmrFse5CTzTOVQ/Jses+tJm1k82vJj2FTxWgwIVCtwHvDi67gaeS38URF3lQhxe8DyQ93C4zs2gzC7texfnIfZKTl+oP8u9b+eAZHS1GpMshs5DD6f9b9iikrDmL8gr0YO3cX5m0/DTV3D1QEFSBwfTvdBRk1Etwf6YbHlAaTyfzdqRh0HLMMsm4f4NVF+6TcikfZeACU978BweyumwJkulqwbn1a2ndhv7wdsiBq4FjQGJZe/xDkrO6JnMWtYZpbF6aPQmCarYNlVTfIArlriVwa7iawZsKZQd/tJVM4xcGrwHVoUFXUImRHiL5DSejZsQFmkFYwkPkhVgLaxMaFXhpH0ww3DHk27ttyWKyYMqgdMjdPcF2kCH4hE5BL0EmOfKOqZXPUs+neTP/OjVCVGhqrR1CDw/kD3v9O2iuaPceioe33CXTdZmHYnB2YuHg/3l24H5/N24svluzHwbNx5T79pSJR0/vf9f7jyL6WKgZN3HDd5dXVOMTPszdmbzwuCt6VBI9BxRWI6qH34DR5dLZzhFGhgf38RimBksjnI9tQ2mNIa3GA5fp+CKnn6MXHk6lEx+SQZjOmw2GIIXOVGle1HvK7nyVzNhuKKo2kc8sP7xqOKrGdgxEhAVhVyJT4gnhrxAPo0qoejJJJykJrI23CkTvhJ2qdPLd9b8C0aQLeGVF80+1CbArH1YHMHHRvU7wxbAXD1cfFvDFdkWPMNaH8yG+JjTVg5a4zUkrBLN1+Ct3Gr4RcrRLXYVEHqNG4VijWzOgPAz2jcGIGFr7QDYYyRlUrm64tayN2y4sI1ShhJN87y2zLEw3lcabcB6gNC0L9ofOQk1MKH1wgYXZSfXNv5GuJaYUgr9KQDvGIJCqUEFI83pOdBdKzepPG4hB+l/fg1/W/UPWcB/XQ/dBMuA7/ty0ImEL+4mQ7/J87B3X3WSSrFRMw9CpwQ7o2FW13FflWe36+LKWWjJ9mDULjsABkWWziS3GSiVZtyOdSbtlYuP0M1BpqveTyYo8yKQ79SMtVqxKUJ3QfGBGEEXN2SnsFIWDU53sQWCsMSvI5uD6yX7j7vYF4/MHG0EkRRbEOFV6PbksiI4IRt2IULqwajafua4DMlEwYkzPzmJpi0EurxugvdkkpJUBGlhRVRZ5pyZvAy5RRWkFwEykj8/CmuciQP+dM/1PaIRRc5h75/NWeDb/2E6BsMxoq8gkVtTuLE1oL61oob7wKXG3uT6FKx/PVFu8o/cI9fyx7Dn7U4nHoW6NUIDHLigFvrZdySwcvjX4pzrWgTLum5Sdsbha/2J387Vwtxx3U3GaP+aRgodv7WwxsVpu4ejTDGkDurxGf2ROBAzy5CvWOoyE1KEsn9Yaw6zVsndkfKqojPIPBjYb8u12/FdLZXgCax76B9qVkaMfFuLaXU6Duv1bK9Q73qXkGuUR4bpyEwLMJhPx9aXJSnrfOs6tMvAocM7JPS+TwyAgykXb8Gi2llhzT9y8hJzZNbNh5aNOG/RexaMsJV2Yp+HQTnUsV2ZJtwYAujaXU8qN3+wa4q6ZeXFbBDZtMX64/ht8ueZ/7dDmJfAHStp7wQrb58dfeGqV0T/250+jdoQHS149D00j9zRkU/CQ8Iqai4ZIVshKpjD3K3EGWVFhufVDW70XHcKhf+j1UzqxFHVd+dO2XJ7Li++UFCtzkp++HI8kIf/LjXv6yFGaCG/aDNk9AZnSqWFA8ROjZd7/HycuJrvwSMmXlYQSQf0S2Kl59vPChVqVly9T+sBpNngYJtNX16PjKGmkvL57C6YYbGO6c96QKm5Ye9ZFD3iYPDXG7wYsrnbhQ+ATLQW3q3lxWkP9wS6i0MFFJeOzt9ZA/MhsBfeeIm+zhD9HvTe9z4Rgh8wb578lUgLkWBGsueQ2PxYK1oUBwAGdICfQ+NDrYthdvdrtgjIFgKnrggMDdDtbiz/4oUODqUQWrE1WFGgYZzl1IKLLgCyMyPAgbPhyIjNh00ewOrheOe4ct9FpRC2PuxhNi9NRss2NAd/IzK4jGUWF4skczZHj0QalIOKwKGZqPWCSl5FIrPJhKPlc8WWsJJos448ATsbA9WmUlCeSVG5UT2SsN564lo3XfjzFwxiYp5VZSWNikR7IZsvH2kOJPCL6Jn4J8MjXk5APyxiP37dKMDm9YVj0MeQgPYJbKnIRNpgmHolauwHG7pmg6gkxPj1EgCrom+XE585t6jck4TWmw/jwb5oV3I3tGbdiOfCzl5JI/ai9TkxAffE/ac8FHOG94H4dcoMAxa6b0hSnegMDaVXDfhK+l1NLx2AONMeWp+2BMzRQLQxMZguoDP3NlFpNxc39EkE4LR4IRi18p/d8xKA4rXusj+mDcAe4mWOOHsylZiBjyOUwe0bjG1Dgxni8jkITwEWq5XyLf7+n3tkBFFbfn2BXQevTDcYAl1WxD99e+xUdksj7+1jpxxeXbhXdWHoGieU1sPHFN7OweNWcHLnosMRGTlIFVe84hkHw37kd9tFND9H+g5KF0tkLZ/83duGK6TAEuUfv5DbD/vgyWnRNhmh1IfgpZR3JJIKnMHWkJUA+/dRC4X+9P4TTmbdBkfnS+xYCcGTLkfFoTOUvbw/RZFLLfp3f9YRgch2fAGXcGqvsGwa/rVOmsXAR2FTxkTqbUQEg4huyZMmTTb8t+l647hSyXz7w3PIUKXLsmkWjVuBpsVOksVPme/0/hc46K4p3nHkSXVnVgzDJDTaVqJL+nw/iVUm7hdJnwDeTkS2Vm5mAcmZI6NitLAWtsdpvcm6eJlx/jt2NgoQaCJ6O6D+P7ZlHrGPDwLFQd8gXW7T9PGrEK2jaIEP+CkBsOtqhIQOfuu4DlR6Nhp+tMG9cdYRplnr4+PQnggT8TMWnOdvx8JQV6buHdiL+Vf6j7uyvZKx7HiMcVcrwrO2855D80KT0bm3acFgUgWOsnrty2aP8FNHpyHmQdZ0B2/7uoTQ2mWaFAVnIGetxTE5tn9JfOzovrXq6b8If7txYLpw2W1QNg3TkGznNfQe4fCpmKTUUBgtkAR2oMNKPPQq6rKZ2QC99B8+xROOKv03XYmnLdU0aajkeosLEhy4ohgXdCEVgNULOfp4Z61DGoB3wrHpsfeUhzundey0zmFwBFSBQUZLLyVB2BXDr1YyWYLeDJ8QUjYCHTSE+Vff66Yzh6Nk7KKR0/ffA4Gupd3QU6ahl/ORuLd5bul3K9M+2rA9h/Lk7sbFU7BHxaijUpmSTSTra4NBji0sXNHmdAUpprESNv+JFZY905CSEqhbjkOjcUvGpXDmklhAYh6Y8bOH3VZb8fmfsUIvyUMJL25cgdCx+vpW8nC6Guzh/R617A1Cfvww9THxODSEZjjngtQ2IGrLRNGfkAYr5+XrwWI2pW+q1G+p28ZdAm0O+1emhcT4xs/t7gZzOIz2alLZ7HQnohmUw/Z55ySEdCWt4BuBHku/+8eCTqhWhhuJJElkmWOJxOTulyntMWRpqCGpTs2FR89kIP7Jw1WDozL3anE9nx6cgkd0J8FvoEfWZzGUqYzHbYqCyM5Jfz5qDv7gEJopD4uYSE/TSnKQmOlGtwZiZA0WIUAqYKUFQt2L1QRLaFdtI1MiEVcPJ55jSxP04cTWKlZ8pJgyM5Fk5lEPwG/QDtKylQVG8tnX0r6gGr4YiNp2sY6RpZ9JlB10iha9A9qG4qOk1BwDQBqnbjpDPyUqwZ39sOXULv19cipHYo0i8m4Pym8WgUWbalzv17fwRZoD/UpDkN0UnYO284Hmzhsd6gxOItJ/Dvj7ZDVyu0zEudx1DFzrHabkbSeFxiEAl99WIsysN/SnnLoYswkt/Jgy505He0aVID3Vrm7XjfeuQydp68BjMdVJsq54ied6M6V05PqMjnrDsKi9WBextVxyNt6koZubBVcflGuviHLEXoLfEQuPpkirPvl59UEmAWJPfx/Fp5dbNaXkbi8JIIPOWI/wotw+Wg1ahQk33RAthw8KI4Cz0h00Ia2o4Q0sRtG1TFgKJMSPrdF2NSqMxZnbiS2A+vU1V/c9TN6p/O07OmwT0dii2ARjVCMOihJvzjYNn7umgKypSk2YIjoahLPhwHRUqIMysRjjNfk38dTy0B+X10DZmuAZR3FzzT3Rs8a8F66H3ISNAENTVAwXXpGsNI8xYdMCr2Egsj3/8eS/adR0hoINIvJeDM2rFoVsqKzzio4iof+gBBtcNENWu8moy07aRNSBt4wpMou7xI/iNVwGVT/4lhj9wt5fjwcedRojVN2pG/9Sv5G+Jf0LmUiHmT+2AUr6JVSnh5gVr9PkEw+UDcemeTn8Odqvl5c8FPUFJLN/35rlKKDx93JiUSOKYXmZbbf70ijvTnwcf3NKyG3+cPl3JLzurdZ/HEjC3Q1dAji7ReHdJwfxZjsqcPH3citzoDRbDtg0Hk/HeCgczKIBK6C+Rsy9pNx9g526QjSsaQbs3I6e4mBhAC/ZS4TE71ix/vkHJ9+PhrUWIN5yY63oB6T84TI1Vl+aP6i374HavIaT4UnSz2e/Gk6IzfrmPlf4dgaK8W0lE+fPw1KLXAuZm25ACmL9gDhOsQFKiGze6AOdMCpGTALzIErRtEoEPNMHE5Aw5SZVsdOHQ9GaejU+EgoeXl1fx5hAFlZvOgYRLcpZP7YrgvOOLjL0iZBc7N1GUHMGPVL7xmAWShAaT4lGKnKY+vszmFm6MweNgTrxnCIWm7QxBH/yMli3w4HT4c3hnP9WkpHufDx1+RchM4N5di0jB/2ylxXcSTp6Tlx1h98W1YxTG8kI5KgU6tovBw6zoY37cl9MGVs0KYDx//T8pd4LwRz6MUxBHlgjipNUIf4Mrw4eNvRqUInA8fPlyUuFvAhw8fpQX4HzGcDWUNHMFHAAAAAElFTkSuQmCC";
    var pageContent = function (data) {
      // HEADER

      if (base64Img) {
        doc.addImage(base64Img, 'PNG', (pageWidth / 2) - 50, 10, 160, 30);
      }
      // doc.text("Rahisibet", data.settings.margin.left + 80, 22);

      // FOOTER
      var str = "" + data.pageCount;
      // Total page number plugin only available in jspdf v1.0+
      if (typeof doc.putTotalPages === 'function') {
        str = str;
      }
      doc.setFontSize(7);
      doc.text(str, pageWidth / 2, pageHeight - 15, 'center');
    };


    doc.autoTable(col, rows, {
      // head: [col],
      // body: rows,
      theme: 'grid',
      styles: { fontSize: 6.5, margin: 10 },
      headStyles: { fillColor: [7, 59, 107], textColor: [255, 255, 255], halign: 'center', valign: 'middle' },
      bodyStyles: { halign: 'center' },
      margin: { top: 50 },
      showHead: 'firstPage',
      didDrawPage: pageContent
    });

    doc.save('jackpot.pdf');
  }

}
