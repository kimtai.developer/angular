import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.css']
})
export class TermsAndConditionsComponent implements OnInit {

  constructor(private titleService: Title) { 
    this.setTitle('Rahisibet - terms and conditions');
  }

  ngOnInit() {
  }

  public setTitle( newTitle: string) {  this.titleService.setTitle( newTitle );  }
  
  toggleAccordian(event) {
    var element = event.target;
    element.classList.toggle("active");
        
    var panel = element.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }
}
