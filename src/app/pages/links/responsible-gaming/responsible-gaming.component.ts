import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-responsible-gaming',
  templateUrl: './responsible-gaming.component.html',
  styleUrls: ['./responsible-gaming.component.css']
})
export class ResponsibleGamingComponent implements OnInit {

  constructor(private titleService: Title) { 
    this.setTitle('Rahisibet - responsible gaming');
  }

  ngOnInit() {
  }

  public setTitle( newTitle: string) {  this.titleService.setTitle( newTitle );  }

  toggleAccordian(event) {
    var element = event.target;
    element.classList.toggle("active");
        
    var panel = element.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }
}
