import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-how-to-play',
  templateUrl: './how-to-play.component.html',
  styleUrls: ['./how-to-play.component.css']
})
export class HowToPlayComponent implements OnInit {

  div_name : string = 'reg_sms';

  constructor(private titleService: Title) { 
    this.setTitle('Rahisibet - How to play');
  }

  ngOnInit() {
  }

  public setTitle( newTitle: string) {  this.titleService.setTitle( newTitle );  }

  showdiv(flag){
    this.div_name = flag;
  }
  
  toggleAccordian(event) {
    var element = event.target;
    element.classList.toggle("active");
        
    var panel = element.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }

}
