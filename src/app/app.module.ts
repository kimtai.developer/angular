import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthModule } from './core/auth/auth.module';

import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { httpInterceptorProviders } from './http-interceptors/index';
import { HeaderComponent } from './core/header/index/index.component';
import { SidebarComponent } from './core/sidebar/index/index.component';
import { BetSlipComponent } from './core/betslip/index/index.component';
import { FooterComponent } from './core/footer/index/index.component';
import { TermsAndConditionsComponent } from './pages/links/terms-and-conditions/terms-and-conditions.component';
import { HowToPlayComponent } from './pages/links/how-to-play/how-to-play.component';
import { ResponsibleGamingComponent } from './pages/links/responsible-gaming/responsible-gaming.component';
import { FeedsComponent } from './pages/user/feeds/feeds.component';
import { FilterPipe } from './pipes/markets.pipe';
import { ProfileComponent } from './pages/user/profile/profile.component';
import { BetsComponent } from './pages/user/bets/bets.component';
import { BetsPlacedComponent } from './pages/user/bets-placed/bets-placed.component';
import { PagerService } from './services/pager.service';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { AutobetComponent } from './pages/home/autobet/autobet.component';
import { JackpotComponent } from './pages/home/jackpot/jackpot.component';
import { Title }  from '@angular/platform-browser';
import { MyDatePickerModule } from 'mydatepicker';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { InviteAFriendComponent } from './pages/user/invite-a-friend/invite-a-friend.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    BetSlipComponent,
    FooterComponent,
    TermsAndConditionsComponent,
    HowToPlayComponent,
    ResponsibleGamingComponent,
    FeedsComponent,
    FilterPipe,
    ProfileComponent,
    BetsComponent,
    BetsPlacedComponent,
    AutobetComponent,
    JackpotComponent,
    InviteAFriendComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    AuthModule,
    FormsModule,
    HttpClientModule,
    MyDatePickerModule,
    SweetAlert2Module.forRoot(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  providers: [httpInterceptorProviders, PagerService,Title,{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent],
})
export class AppModule { }
