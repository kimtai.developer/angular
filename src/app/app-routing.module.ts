import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HowToPlayComponent } from './pages/links/how-to-play/how-to-play.component';
import { ResponsibleGamingComponent } from './pages/links/responsible-gaming/responsible-gaming.component';
import { TermsAndConditionsComponent } from './pages/links/terms-and-conditions/terms-and-conditions.component';
import { FeedsComponent } from './pages/user/feeds/feeds.component';
import { ProfileComponent } from './pages/user/profile/profile.component';
import { AuthGuard } from './guards/auth.guard';
import { BetsComponent } from './pages/user/bets/bets.component';
import { BetsPlacedComponent } from './pages/user/bets-placed/bets-placed.component';
import { AutobetComponent } from './pages/home/autobet/autobet.component';
import { JackpotComponent } from './pages/home/jackpot/jackpot.component';
import { InviteAFriendComponent } from './pages/user/invite-a-friend/invite-a-friend.component';


const routes: Routes = [
  { path: 'how-to-play', component: HowToPlayComponent },
  { path: 'terms-and-conditions', component: TermsAndConditionsComponent },
  { path: 'responsible-gaming', component: ResponsibleGamingComponent },
  { path: 'sport', component: FeedsComponent, runGuardsAndResolvers:'always' },
  { path: 'league', component: FeedsComponent, runGuardsAndResolvers:'always' },
  { path: 'profile', component:ProfileComponent,canActivate: [AuthGuard] },
  { path: 'slip', component:BetsPlacedComponent,canActivate: [AuthGuard], runGuardsAndResolvers:'always' },
  { path: 'betslip', component:BetsComponent,canActivate: [AuthGuard] },
  { path: 'autobet', component:AutobetComponent },
  { path: 'jackpot', component:JackpotComponent },
  { path: 'invite-a-friend', component:InviteAFriendComponent,canActivate: [AuthGuard] },
  { path: '', component: FeedsComponent },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
