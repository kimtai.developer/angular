// Define a type for the display market items
export interface Market {
  id: number;
  name: string;
  key: string;
  handicap: string;
  mobile: boolean;
  selections: Selection[];
}

export interface Selection {
  id: number;
  name: string;
}

export interface Sport {
  id: number;
  name: string;
  frName: string;
  markets: Market[];
}

// -------Match-----
// Define a type for the match items
export interface MatchMarket {
  handicap: string;
  name: string;
  id: number;
  marketId: string;
  odds: Odds[];
}

export interface Odds {
  status: string;
  name: string;
  id: number;
  marketId: string;
  odds: number;
  outcomeId: number;
}

export interface Match {
  id: number;
  sportId: number;
  sportName: string;
  tournamanent: string;
  scheduled: string;
  country: string;
  home: string;
  away: string;
  marketCount: number;
  markets: MatchMarket[];
}

// -------Betslip-----
// Define a type for the betslip items
export interface Betslip {
  id: number;
  home: string;
  away: string;
  market: string;
  selected: string;
  odd: number;
  oddId: string;
  schedule: Date;
  type: string;
}

// -------Menu-----
// Define a type for the menu items
export interface League {
  id: number;
  name: string;
  isFeatured: number;
}

export interface Country {
  id: number;
  name: string;
  leagues: League[];
  regionSportId: string;
}

export interface Menu {
  id: number;
  name: string;
  country: Country[];
}

// -------JetX-----
export interface JetX {
  url: string;
  gameCategory: string;
  gameName: string;
  token: string;
  portalName: string;
  returnUrl: string;
  lang: string;
}

// -------Aviatrix-----
export interface Aviatrix {
  url: string;
  key: string;
  cid: string;
  lobbyUrl: string;
  isDemo: true;
  isFull: true;
}

// -------Flags-----
export interface Flags {
  [index: string]: string;
}

export type Fibs = {
  id: number;
  nth?: string;
  loading?: boolean;
  time?: number;
  fibNum?: number;
};
export type Msg = {
  fibNum: number;
  time: number;
};

// ------------------------- New types ------------------
export interface Matchtype {
  away: string;
  country: string;
  home: string;
  id: number;
  marketCount: number;
  markets: MarketType[];
  scheduled: Date;
  sportId: number;
  sportName: string;
  tournamanent: string;
}

export interface MarketType {
  handicap: string;
  name: string;
  id: number;
  marketId: string;
  odds: OddType[];
}

export interface OddType {
  status: string;
  name: string;
  id: string;
  odds: number;
  marketId: number;
  outcomeId: number;
}

export type Data = Record<string, any>;

export interface Menu {
  id: number;
  name: string;
  country: Country[];
}

export interface Country {
  id: number;
  name: string;
  leagues: League[];
  regionSportId: string;
}

export interface League {
  id: number;
  name: string;
  isFeatured: number;
}

export interface DisplayMarkets {
  id: number;
  name: string;
  frName: string;
  markets: MarketDisp[];
}

export interface MarketDisp {
  id: number;
  name: string;
  key: string;
  handicap: string;
  mobile?: boolean;
  selections: Selections[];
}

export interface Selections {
  name: string;
  id: string | number;
}

export type CasinoType = { image: string; name: string };
