export class Profile {
    user_id : number;
    name: string;
    surname: string;
    email:string;
    gender:string;
    zone:string;
    town:string;
    
    constructor() {}
}