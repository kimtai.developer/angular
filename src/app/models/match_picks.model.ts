export class Match_Picks {
    name : string;
    competition_id : string;
    outcome_id : number;
    market : string;
    pick : string;
    game_id : string;
    odd : number;
    scheduled : string;
}