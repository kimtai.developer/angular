export class User {
    phone: number;
    password: number;
    confirmationPassword: number;
    zone:string;

    constructor(phone: number, password: number, confirmationPassword: number,zone:string) {
        this.phone = phone;
        this.password = password;
        this.confirmationPassword = confirmationPassword;
        this.zone = zone;
      }
}