import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ 
    name: 'marketfilter'
  })
  export class FilterPipe implements PipeTransform {
    transform(arr:any, searchValue: number) { 
      if (!searchValue) return arr;
        
      let data = arr.some(ob => ob['sr_outcome_id'] === searchValue );
    
    if (data) {
        return true;
    } else {
        return false;
    }
    }
  }