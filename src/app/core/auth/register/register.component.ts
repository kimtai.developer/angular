import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

import { MustMatch } from '../../../helpers/must-match.validator';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';
import { Title }     from '@angular/platform-browser';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  error: {};
  zones: any = []; 

  constructor(
    private formBuilder: FormBuilder, 
    private authService: AuthService, 
    private router: Router, 
    private userservice: UserService,
    private titleService: Title) { }

  ngOnInit() {
    this.setTitle('Rahisibet - register');
    this.userservice.getZones().subscribe(data => { this.zones = data; });
    this.registerForm = this.formBuilder.group(
      {
        phone: [null, [Validators.required, Validators.pattern("^[0-9]*$"),Validators.minLength(8)]],
        password: [null, [Validators.required,Validators.pattern("^[A-Za-z0-9]*$"), Validators.minLength(6),Validators.maxLength(6)]],
        confirmPassword: [null, [Validators.required, Validators.pattern("^[A-Za-z0-9]*$"), Validators.minLength(6),Validators.maxLength(6)]],
        zone: ['', [Validators.required]]
      },{
        validator: MustMatch('password', 'confirmPassword')
    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  public setTitle( newTitle: string) {  this.titleService.setTitle( newTitle );  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    let user = new User(
      this.registerForm.value.phone,
      this.registerForm.value.password,
      this.registerForm.value.confirmPassword,
      this.registerForm.value.zone
      )
   
    console.log(user)
    this.authService.register(user).subscribe(
      data => {
        if(data){
          this.router.navigate(['auth/verification']);
          this.authService.changeMessage("registered_account");
          this.authService.writePhone(this.registerForm.value.phone);
        }
      },
      error => this.error = error);
    
  }

}
