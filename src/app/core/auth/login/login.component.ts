import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router, ActivatedRoute } from "@angular/router";
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  logged: boolean = false;
  loginMainForm: FormGroup;
  submitted = false;
  error: {};
  loginError: string;
  return: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title) { }

  ngOnInit() {
    this.setTitle('Rahisibet - Login');
    this.loginMainForm = this.formBuilder.group(
      {
        phone: [null, [Validators.required, Validators.pattern("^[0-9]*$"), , Validators.minLength(8)]],
        password: [null, [Validators.required, Validators.minLength(6),Validators.maxLength(6),Validators.pattern("^[A-Za-z0-9]*$")]]
      });
    // Get the query params
    this.route.queryParams.subscribe(params => this.return = params['return']);
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginMainForm.controls; }

  public setTitle(newTitle: string) { this.titleService.setTitle(newTitle); }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginMainForm.invalid) {
      return;
    }
    this.authService.login(this.loginMainForm.value.phone, this.loginMainForm.value.password).subscribe(
      () => {
        if (this.authService.isLoggedIn) {
          this.logged = true;
          if(this.return) {
            this.router.navigateByUrl(this.return);
            this.authService.changeMessage("");
          }
          else if(localStorage.getItem('reload-url') != null) {
            let url = JSON.parse(localStorage.getItem('reload-url'));
            this.router.navigateByUrl(url);
          }
        } else {
          this.error = true;
        }
      },
      error => {
        if (error == "verify_account") {
          this.router.navigate(['/auth/verification']);
          this.authService.changeMessage("verify_account");
          this.authService.writePhone(this.loginMainForm.value.phone);
        } else {
          this.error = error;
        }
      });
  }
}
