import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  submitted = false;
  error: {};
  

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router,private titleService: Title) { }

  ngOnInit() {
    this.setTitle('Rahisibet - forgot password');
    this.forgotPasswordForm = this.formBuilder.group(
    {
      phone: [null, [Validators.required, Validators.pattern("^[0-9]*$"),Validators.minLength(8)]]
    });
  }
 
  // convenience getter for easy access to form fields
  get f() { return this.forgotPasswordForm.controls; }

  public setTitle( newTitle: string) {  this.titleService.setTitle( newTitle );  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.forgotPasswordForm.invalid) {
      return;
    }

    this.authService.forgotpassword(this.forgotPasswordForm.value.phone).subscribe(
      data => {
        if(data){
          this.router.navigate(['auth/reset-password']);
          this.authService.writePhone(this.forgotPasswordForm.value.phone);
        }
      },
      error => this.error = error);
  }

}
