import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

import { MustMatch } from '../../../helpers/must-match.validator';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  resetForm: FormGroup;
  submitted = false;
  error: {};
  phone:string;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router,private titleService: Title) { }

  ngOnInit() {
    this.setTitle('Rahisibet - reset password');
    this.authService.currentNumber.subscribe(phone => this.phone = phone);
    this.resetForm = this.formBuilder.group(
      {
        phone: [this.phone, [Validators.required, Validators.pattern("^[0-9]*$"),Validators.minLength(8), Validators.maxLength(10)]],
        resetCode: [null, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(6), Validators.maxLength(6)]],
        password: [null, [Validators.required, Validators.pattern("^[A-Za-z0-9]*$"),Validators.minLength(6), Validators.maxLength(6)]],
        confirmPassword: [null, [Validators.required, Validators.pattern("^[A-Za-z0-9]*$"), Validators.minLength(6), Validators.maxLength(6)]]
      }, {
        validator: MustMatch('password', 'confirmPassword')
      });
  }

  // convenience getter for easy access to form fields
  get f() { return this.resetForm.controls; }

  public setTitle( newTitle: string) {  this.titleService.setTitle( newTitle );  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.resetForm.invalid) {
      return;
    }

    this.authService.resetpassword(this.resetForm.value.phone,this.resetForm.value.password,this.resetForm.value.confirmPassword, this.resetForm.value.resetCode).subscribe(
      data => {
        if(data){
          this.router.navigate(['/']);
        }
      },
      error => this.error = error);

  }

}
