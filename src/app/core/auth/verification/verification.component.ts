import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {

  verifyForm: FormGroup;
  submitted = false;
  error: {};
  message: string;
  phone:string;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router,private titleService: Title) { }

  ngOnInit() {
    this.setTitle('Rahisibet - verification');
    this.authService.currentMessage.subscribe(message => this.message = message);
    this.authService.currentNumber.subscribe(phone => this.phone = phone);

    this.verifyForm = this.formBuilder.group(
      {
        phone: [this.phone, [Validators.required, Validators.pattern("^[0-9]*$")]],
        verificationCode: [null, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(6)]],
      }
    );     
  }

  // convenience getter for easy access to form fields
  get f() { return this.verifyForm.controls; }

  public setTitle( newTitle: string) {  this.titleService.setTitle( newTitle );  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.verifyForm.invalid) {
      return;
    }

    this.authService.verification(this.verifyForm.value.phone, this.verifyForm.value.verificationCode).subscribe(
      data => {
        if (data) {

          if(data.type == 'INVITE FRIEND')
          {
            this.router.navigate(['auth/reset-password']);
            this.authService.writePhone(this.verifyForm.value.phone);
          }
          else
          {
            this.router.navigate(['/']);
            this.authService.changeMessage("");
            this.authService.writePhone('');
          }
        }
      },
      error => this.error = error);
  }

}
