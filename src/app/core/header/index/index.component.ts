import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: 'app-header',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class HeaderComponent implements OnInit {

    loggedIn: boolean = this.authService.isLoggedIn();
    subscription: Subscription;
    balanceSubscription: Subscription;
    betcountSubscription: Subscription;
    logout: string;

    loginForm: FormGroup;
    submitted = false;
    returnUrl: string;
    error: {};
    loginError: string;
    finance: any;

    balance: any;
    bonus: number;
    betcount: number = 0;
    time = new Date();
    timer;
    return: string = '';
    user_id: number;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private userService: UserService,
        private router: Router,
        private route: ActivatedRoute,
        public translate: TranslateService
    ) {

        this.subscription = this.authService.CheckLoggedIn().subscribe(flag => {
            this.loggedIn = flag;

            if (localStorage.getItem("currentUser") != null) {
                this.finance = JSON.parse(localStorage.getItem("currentUser"));
                this.user_id = this.finance.id;
                this.balance = this.finance.account.account;
                this.bonus = this.finance.account.bonus;
            }

        });

        this.betcountSubscription = this.userService.getBetcount().subscribe(data => {
            this.betcount = data;
        });

        this.balanceSubscription = this.authService.getBalance().subscribe(data => {
            if (data != null) {
                this.balance = data.account.account;
                this.bonus = data.account.bonus;
            }
        });

        translate.getLangs();
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            phone: [null, [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(8)]],
            password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(6),Validators.pattern("^[A-Za-z0-9]*$")]]
        });

        // this.date.toLocaleTimeString();
        this.timer = setInterval(() => {
            this.time = new Date();
        }, 1000);

        // Get the query params
        this.route.queryParams.subscribe(params => this.return = params['return']);
        this.error = false;
    }

    ngOnDestroy() {
        clearInterval(this.timer);
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        } else {
            this.authService.login(this.loginForm.value.phone, this.loginForm.value.password).subscribe(
                data => {
                    if (this.authService.isLoggedIn && !(this.authService.isTokenExpired())) {
                        if (this.return) {
                            this.router.navigateByUrl(this.return);
                            this.authService.changeMessage("");
                        }
                        else if (localStorage.getItem('reload-url') != null) {
                            let url = JSON.parse(localStorage.getItem('reload-url'));
                            this.router.navigateByUrl(url);
                        }
                    } else {
                        this.error = true;
                    }
                },
                error => {
                    this.error = 'verify account';
                    if (error) {
                        this.router.navigate(['/auth/verification']);
                        this.authService.changeMessage("verify_account");
                        this.authService.writePhone(this.loginForm.value.phone);
                    } else {
                        this.error = error;
                    }
                }
            );
        }
    }

    getBalance() {
        this.userService.getUser(this.user_id).subscribe(data => {
            this.authService.setBalance(data)
        });
    }

    onLogout() {
        this.authService.logout().subscribe(data => { });
        this.error = false;
        this.router.navigate(['/']);
    }

    useLanguage(language: string) {
        this.translate.use(language);
    }
}
