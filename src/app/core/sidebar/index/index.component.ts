import { Component, OnInit } from '@angular/core';
import { DatashareService } from 'src/app/services/datashare.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class SidebarComponent implements OnInit {

  public menu: any = [];
  public top5: any = [];

  minisidebar = new Subscription;
  top5leagues = new Subscription;

  public load: boolean = false;
  

  constructor(private datashare: DatashareService, private router: Router,public translate: TranslateService) {

    this.minisidebar = this.datashare.getCategorySubject().subscribe(data => {
      this.menu = data.data;
      this.load = false;
    });
    
    this.top5leagues = this.datashare.getTop5LeaguesSubject().subscribe(data => {
      this.top5 = data;
    });
    
  }

  ngOnInit() { 
    this.load = true;
    this.datashare.getCategories().subscribe(data => { });
    this.datashare.getTop5Leagues().subscribe(result => { })
   }

  getLeagueGames(id) {
    localStorage.setItem('category', JSON.stringify(id));
    localStorage.setItem('sport', JSON.stringify(null));

    if (localStorage.getItem("category") != null) {
      this.router.navigate(['league']);
    }
  }

  getUpcomingGames(sport_id) {
    localStorage.setItem('sport', JSON.stringify(sport_id));
    localStorage.setItem('category', JSON.stringify(null));

    if (localStorage.getItem("sport") != null) {
      this.router.navigate(['sport']);
    }
  }
}
