import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth.service";
import { Subscription } from "rxjs";
import { UserService } from "src/app/services/user.service";
import { Betslip } from "src/app/models/betslip.model";
import { Match_Picks } from "src/app/models/match_picks.model";
import { AutobetService } from "src/app/services/autobet.service";
import { JackpotService } from "src/app/services/jackpot.services";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import * as jsPDF from "jspdf";
import "jspdf-autotable";

@Component({
  selector: "app-betslip",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.css"],
})
export class BetSlipComponent implements OnInit {
  loggedIn: boolean = this.authService.isLoggedIn();
  subscription: Subscription;
  subscriptionBetslip: Subscription;
  maxBetslipSubscription: Subscription;
  subscriptionAutobet: Subscription;
  subscriptionJackpot: Subscription;

  betslip: any = [];

  totalodds: number = 1;
  possiblewin: number = 1;
  amount: number = 500;
  balance: number = 0;
  betcount: any;
  bettype: string = "";

  beterror: boolean = false;
  maxbeterror: boolean = false;
  autobeterror: boolean = false;
  automaxbeterror: boolean = false;
  jackpoterror: boolean = false;
  jackpotmaxbeterror: boolean = false;

  balanceerror: boolean = false;
  maximumBets: boolean = false;

  betsliperror: boolean = false;
  betslipdetailserror: boolean = false;

  autobet_flag: boolean = false;
  betslip_flag: boolean = false;
  jackpot_flag: boolean = false;

  bet_success: boolean = false;
  bet_error: boolean = false;
  oddschange: boolean = false;
  user_id: number;
  display: boolean = false;
  betstop: boolean = false;
  betstop_all: boolean = false;
  suspended_markets: boolean = false;
  oddschange_data: any = [];
  started_data: any = [];
  bet_delay: boolean = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private autobet: AutobetService,
    private jackpot: JackpotService,
    private router: Router,
    public translate: TranslateService
  ) {
    this.subscription = this.authService.CheckLoggedIn().subscribe((flag) => {
      this.loggedIn = flag;
    });

    this.maxBetslipSubscription = this.userService
      .getMessage()
      .subscribe((data) => {
        this.maximumBets = data;
      });

    this.subscriptionBetslip = this.userService
      .getBetslipSubject()
      .subscribe((data) => {
        this.autobet_flag = false;
        this.betslip_flag = true;
        this.jackpot_flag = false;
        this.bettype = "betslip";

        if (data != null && data.length > 0) {
          this.betslip = data;
        } else {
          this.betslip.length = 0;
          this.display = false;
        }

        this.betcount = this.betslip.length;
        this.totalodds = this.fixDecimals(data);
        this.possiblewin = this.getPossibleWin(
          this.amount,
          this.fixDecimals(data)
        );
        this.amount = data == null ? 500 : this.amount;

        this.betcount = data == null ? 0 : data.length;
        this.userService.setBetcount(this.betcount);

        // Create betslip to server
        this.createBetslip(data, this.amount);
      });

    // autobet
    this.subscriptionAutobet = this.autobet
      .getAutobetSelect()
      .subscribe((data) => {
        this.amount = 1000;
        this.bettype = "autobet";

        if (data != null && data.length > 0) {
          this.betslip = data;
        } else {
          this.betslip.length = 0;
          this.display = false;
        }

        this.betslip_flag = false;
        this.autobet_flag = true;
        this.jackpot_flag = false;

        this.betcount = this.betslip.length;
        this.amount = data == null ? 1000 : this.amount;

        // Create betslip to server
        this.createBetslip(data, this.amount);
      });

    // jackpot
    this.subscriptionJackpot = this.jackpot
      .getAutobetSelect()
      .subscribe((data) => {
        this.amount = 2000;
        this.bettype = "mega jackpot";

        if (data != null && data.length > 0) {
          this.betslip = data;
        } else {
          this.betslip.length = 0;
          this.display = false;
        }

        this.betslip_flag = false;
        this.autobet_flag = false;
        this.jackpot_flag = true;

        this.betcount = this.betslip.length;
        this.amount = data == null ? 2000 : this.amount;

        // Create betslip to server
        this.createBetslip(data, this.amount);
      });

    translate.getLangs();
  }

  ngOnInit() {
    if (localStorage.getItem("currentUser") != null) {
      let user = JSON.parse(localStorage.getItem("currentUser"));
      this.user_id = user.id;
    }
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  createBetslip(data, amount, bettype?: string) {
    if (data != null) {
      let betslip_obj = new Betslip();
      let matchpicks_obj = new Match_Picks();

      betslip_obj.amount = amount;
      betslip_obj.bettype = bettype;
      betslip_obj.multibet = data.length > 1 ? true : false;

      data.forEach((el) => {
        matchpicks_obj.name = el.name;
        matchpicks_obj.competition_id = el.id;
        matchpicks_obj.scheduled = el.scheduled;
        matchpicks_obj.outcome_id = el.selected_outcome;

        el.markets.map((elem) => {
          if (elem.marketId == el.selected_outcome) {
            elem.odds.map((el) => {
              if (this.translate.currentLang == "en") {
                matchpicks_obj.pick = el.name;
              } else {
                matchpicks_obj.pick = elem.name;
              }

              matchpicks_obj.odd = el.odds;
              matchpicks_obj.market = elem.marketId;
            });
          }
        });
        let tmpBet = Object.assign({}, matchpicks_obj);
        betslip_obj.picks.push(tmpBet);
      });
      return betslip_obj;
    }
  }

  fixDecimals(value: any[]) {
    console.log(value);

    let temp: number = 1;
    if (value == null) {
      return temp;
    }

    value.forEach((el) => {
      el.markets.map((elem) => {
        if (el.selected_outcome == elem.marketId) {
          console.log(el.odds);

          temp *= <number>elem.odds;
        }
      });
    });

    return parseFloat(temp.toFixed(2));
  }

  getPossibleWin(amount: number, totalodds: number) {
    if (amount < 500) {
      this.beterror = true;
      return 0;
    } else {
      this.beterror = false;
      let temp: number = amount * totalodds * 0.95;
      return parseInt(temp.toFixed(0));
    }
  }

  valueChange(value) {
    if (value < 500) {
      this.beterror = true;
      this.maxbeterror = false;
      let temp: number = value * 0;
      this.possiblewin = parseInt(temp.toFixed(0));
      return true;
    } else if (value > 1000000) {
      this.maxbeterror = true;
      let temp: number = value * 0;
      this.possiblewin = parseInt(temp.toFixed(0));
    } else {
      this.beterror = false;
      this.maxbeterror = false;
      let temp: number = value * <number>this.totalodds * 0.95;
      this.possiblewin = parseInt(temp.toFixed(0));
      this.createBetslip(this.betslip, this.amount);
    }
  }

  btnChange(value) {
    this.amount = value;
    this.valueChange(value);
  }

  removeSlip(gameid: number) {
    let betcheck = this.betslip.find((x) => x.id == gameid);
    this.userService.popBestlip(betcheck);
  }

  removeAll() {
    this.betslip.length = 0;
    this.userService.clearBetslip();
  }

  // autobet
  autobetRemoveSlip(gameid: number) {
    let betcheck = this.betslip.find((x) => x.id == gameid);
    this.autobet.popBestlip(betcheck);
  }

  autobetRemoveAll() {
    this.betslip.length = 0;
    this.autobet.clearBetslip();
  }

  autoBetValueChange(value) {
    if (value < 1000) {
      this.autobeterror = true;
      this.automaxbeterror = false;
      return true;
    } else if (value > 1000000) {
      this.automaxbeterror = true;
      return true;
    } else {
      this.autobeterror = false;
      this.automaxbeterror = false;
      this.createBetslip(this.betslip, this.amount);
    }
  }

  autobetPlaceBetslip() {
    let payload = this.createBetslip(this.betslip, this.amount, "autobet");
    this.userService.sendAutoBetslip(payload).subscribe((data) => {
      if (data.success) {
        this.bet_success = true;
        setTimeout(() => {
          this.bet_success = false;
          this.autobetRemoveAll();

          // update balance
          this.userService.getUser(this.user_id).subscribe((data) => {
            this.authService.setBalance(data);
          });
        }, 5000);
      } else {
        this.bet_error = true;
        setTimeout(() => {
          this.bet_error = false;
        }, 5000);
      }
    });
    this.amount = 1000;
  }
  // end of autobet

  // jackpot
  jackpotRemoveSlip(gameid: number) {
    let betcheck = this.betslip.find((x) => x.match_id == gameid);
    this.jackpot.popBestlip(betcheck);
  }

  jackpotRemoveAll() {
    this.betslip.length = 0;
    this.jackpot.clearBetslip();
  }

  jackpotValueChange(value) {
    if (value < 2000) {
      this.jackpoterror = true;
      this.jackpotmaxbeterror = false;
      return true;
    } else if (value > 1000000) {
      this.jackpotmaxbeterror = true;
      return true;
    } else {
      this.jackpoterror = false;
      this.jackpotmaxbeterror = false;
      this.createBetslip(this.betslip, this.amount);
    }
  }

  jackpotPlaceBetslip() {
    let payload = this.createBetslip(this.betslip, this.amount, "jackpot");
    this.userService.sendJackpotBetslip(payload).subscribe((data) => {
      if (data.success) {
        this.bet_success = true;
        setTimeout(() => {
          this.bet_success = false;
          this.jackpotRemoveAll();

          // update balance
          this.userService.getUser(this.user_id).subscribe((data) => {
            this.authService.setBalance(data);
          });
        }, 5000);
      } else {
        this.bet_error = true;
        setTimeout(() => {
          this.bet_error = false;
        }, 5000);
      }
    });
    this.amount = 2000;
  }
  // end of autobet

  //normal bet
  placeBetslip() {
    let payload = this.createBetslip(this.betslip, this.amount, "betslip");
    this.bet_delay = true;

    this.userService.sendBetslip(payload).subscribe((data) => {
      if (data.success) {
        this.bet_success = true;
        setTimeout(() => {
          this.bet_success = false;
          this.bet_delay = false;
          this.removeAll();

          // update balance
          this.userService.getUser(this.user_id).subscribe((data) => {
            this.authService.setBalance(data);
          });
        }, 5000);
      } else {
        if (data.oddschange) {
          this.oddschange = true;
          this.bet_delay = false;
          this.oddschange_data = data.data.oddschange;
        } else {
          let ret = data;

          let existingbetslip = JSON.parse(localStorage.getItem("betslip"));
          if (ret.data.started != undefined) {
            if (ret.data.started.length > 0) {
              if (ret.data.started.length == existingbetslip.length) {
                this.betstop_all = true;
              } else {
                this.betstop = true;
                this.betstop_all = false;
                this.bet_delay = false;
              }
              this.started_data = ret.data.started;
            } else {
              if (ret.data.producer_down.length != existingbetslip.length) {
                this.suspended_markets = true;
                this.started_data = ret.data.producer_down;
              } else {
                this.started_data = ret.data.producer_down;
                this.bet_error = true;
                this.bet_delay = false;
              }
            }
          } else {
            if (!ret.data.amount) {
              this.balanceerror = true;
              this.bet_delay = false;
            }
          }
        }
        setTimeout(() => {
          this.bet_error = false;
          this.oddschange = false;
          this.balanceerror = false;
          this.betstop_all = false;
          this.amount = this.amount;
          this.bet_delay = false;
        }, 5000);
      }
    });
  }

  oddsChange() {
    this.userService.onOddsChange(this.oddschange_data);
    this.oddschange = false;
  }

  onBetstop() {
    this.userService.onBetstop(this.started_data);
    this.betstop = false;
    this.suspended_markets = false;
    this.betstop_all = false;
  }
  //end of normal bet

  login() {
    let url = this.router.url;
    localStorage.setItem("reload-url", JSON.stringify(url));
    this.display = false;
    this.router.navigate(["auth/login"]);
  }

  toggle() {
    this.display = !this.display;
  }

  public generatePDF(bettype) {
    var doc = new jsPDF("l", "pt", "a4");
    if (this.translate.currentLang == "en") {
      var col = ["Id", "Game", "Game ID", "Schedule", "Market", "Odd", "Pick"];
    } else {
      var col = [
        "Id",
        "Jeu",
        "ID du jeu",
        "Programme",
        "Marches",
        "Cote",
        "Choisir",
      ];
    }
    var rows = [];
    let x = 1;

    this.betslip.forEach((el) => {
      let tmp = [];
      tmp[0] = x;

      if (this.translate.currentLang == "en") {
        tmp[1] = el.name;
      } else {
        tmp[1] = el.french;
      }

      tmp[2] = el.game_id;
      tmp[3] = el.scheduled;
      tmp[4] = el.selected_market;

      el.markets[el.selected_market].forEach((elem) => {
        if (elem.sr_outcome_id == el.selected_outcome) {
          tmp[5] = elem.odds;
          if (this.translate.currentLang == "en") {
            tmp[6] = elem.name;
          } else {
            tmp[6] = elem.fr_name;
          }
        }
      });

      x++;
      rows.push(tmp);
    });

    for (let index = 0; index < 3; index++) {
      let xtr = [];

      if (index == 1) {
        xtr[0] = index;
        if (this.translate.currentLang == "en") {
          xtr[1] = "Amount";
        } else {
          xtr[1] = "Montants";
        }
        xtr[2] = "";
        xtr[3] = "";
        xtr[4] = "";
        xtr[5] = "";
        xtr[6] = "BIF " + this.amount;
      } else if (index == 2) {
        xtr[0] = index;
        if (this.translate.currentLang == "en") {
          xtr[1] = "Possible Win";
        } else {
          xtr[1] = "Victoire possible";
        }
        xtr[2] = "";
        xtr[3] = "";
        xtr[4] = "";
        xtr[5] = "";
        if (bettype == "autobet") {
          xtr[6] = "BIF 1,000,000";
        } else if (bettype == "mega jackpot") {
          xtr[6] = "BIF 5,000,000";
        } else {
          xtr[6] = "BIF " + this.possiblewin;
        }
      } else {
        xtr[0] = "";
        xtr[1] = "";
        xtr[2] = "";
        xtr[3] = "";
        xtr[4] = "";
        xtr[5] = "";
        xtr[6] = "";
      }

      rows.push(xtr);
    }

    doc.autoTable({
      head: [col],
      body: rows,
      theme: "grid",
      styles: { fontSize: 10, margin: 10 },
      headStyles: {
        fillColor: [7, 59, 107],
        textColor: [255, 255, 255],
        halign: "center",
        valign: "middle",
      },
    });

    if (this.translate.currentLang == "en") {
      doc.text("Betslip", 40, 20);
      doc.save("betslip.pdf");
    } else {
      doc.text("Coupon de pari", 40, 20);
      doc.save("slip.pdf");
    }
  }
}
