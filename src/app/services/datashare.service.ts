import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, Subject, throwError, BehaviorSubject } from "rxjs";
import { catchError, map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class DatashareService {
  serverUrl: string = "https://test.rahisibet.com/ApiSdk/api/v2";
  errorData: {};
  redirectUrl: string;

  private upcoming_games = new BehaviorSubject<any>([]);
  private all_sports = new BehaviorSubject<any>([]);
  private metadata = new BehaviorSubject<any>([]);
  private top5leagues = new BehaviorSubject<any>([]);
  private slides = new BehaviorSubject<any>([]);

  constructor(private httpClient: HttpClient) {}

  featuredSports(): Observable<any> {
    return this.httpClient
      .get<any>(`${this.serverUrl}/featured?page=0&size=15`, {})
      .pipe(
        map((data) => {
          this.upcoming_games.next(data.data);
          this.metadata.next(data);
        }),
        catchError(this.handleError)
      );
  }

  upcomingSports(sport_id): Observable<any> {
    return this.httpClient
      .post<any>(`${this.serverUrl}/games/upcoming/` + sport_id, {})
      .pipe(
        map((data) => {
          this.upcoming_games.next(data["data"]);
          this.metadata.next(data["meta"]);
        }),
        catchError(this.handleError)
      );
  }

  todayGames(): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/today_games`, {}).pipe(
      map((data) => {
        this.upcoming_games.next(data["data"]);
        this.metadata.next(data["meta"]);
      }),
      catchError(this.handleError)
    );
  }

  getTournamentById(id): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/tournament/` + id, {}).pipe(
      map((data) => {
        this.upcoming_games.next(data["data"]);
        this.metadata.next(data["meta"]);
      }),
      catchError(this.handleError)
    );
  }

  getCategories(): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/menu`).pipe(
      map((data) => {
        data["data"].forEach((el) => {
          el.country.forEach((cat) => {
            cat.code = cat.name.toLowerCase();
          });
        });

        data["data"].forEach((el) => {
          el.country.sort(this.compare);
        });

        this.all_sports.next(data);
      }),
      catchError(this.handleError)
    );
  }

  getSlides(): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/slides`).pipe(
      map((data) => {
        this.slides.next(data["data"]);
      }),
      catchError(this.handleError)
    );
  }

  getTop5Leagues(): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/top_leagues`).pipe(
      map((result) => {
        Object.entries(result).forEach((elem) => {
          elem[1].forEach((el) => {
            if (el.category.country_code != "") {
              el.category.country_code = el.category.country_code.toLowerCase();
            } else {
              el.category.country_code = "default";
            }
          });
        });

        this.top5leagues.next(result["data"]);
      }),
      catchError(this.handleError)
    );
  }

  getGamesByDate(date): Observable<any> {
    return this.httpClient
      .post<any>(`${this.serverUrl}/search/games/by_day`, { to: date })
      .pipe(
        map((data) => {
          this.upcoming_games.next(data["data"]);
          this.metadata.next(data["meta"]);
        }),
        catchError(this.handleError)
      );
  }

  // pagination
  getNextPage(page, path): Observable<any> {
    return this.httpClient.post<any>(path + "?page=" + page, {}).pipe(
      map((data) => {
        this.upcoming_games.next(data["data"]);
      }),
      catchError(this.handleError)
    );
  }

  getByDateNextPage(page, path, date): Observable<any> {
    return this.httpClient.post<any>(path + "?page=" + page, { to: date }).pipe(
      map((data) => {
        this.upcoming_games.next(data["data"]);
      }),
      catchError(this.handleError)
    );
  }

  getNextTournamentPage(page, path): Observable<any> {
    return this.httpClient.get(path + "?page=" + page, {}).pipe(
      map((data) => {
        this.upcoming_games.next(data["data"]);
      }),
      catchError(this.handleError)
    );
  }

  getNextTodayGames(page, path): Observable<any> {
    return this.httpClient.get(path + "?page=" + page, {}).pipe(
      map((data) => {
        this.upcoming_games.next(data["data"]);
      }),
      catchError(this.handleError)
    );
  }
  // end

  getFeaturedGames(): Observable<any> {
    return this.upcoming_games.asObservable();
  }
  getUpcomingGames(): Observable<any> {
    return this.upcoming_games.asObservable();
  }

  getCategorySubject(): Observable<any> {
    return this.all_sports.asObservable();
  }
  getMetaSubject(): Observable<any> {
    return this.metadata.asObservable();
  }

  getTop5LeaguesSubject(): Observable<any> {
    return this.top5leagues.asObservable();
  }

  getSlidesSubject(): Observable<any> {
    return this.slides.asObservable();
  }

  getPrintable(): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/printable/matches`).pipe(
      map((data) => {
        return data["data"];
      }),
      catchError(this.handleError)
    );
  }

  compare(a, b) {
    if (a.country < b.country) {
      return -1;
    }
    if (a.country > b.country) {
      return 1;
    }
    return 0;
  }

  private handleError(error: HttpErrorResponse) {
    // if (error instanceof Error) {
    //     console.log('An error occurred:', error.message);
    // } else {
    //     console.log(`Backend returned code ${error.status}, ` + `body was: ${error.message}`);
    // }
    this.errorData = error;
    return throwError(this.errorData);
  }
}
