import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AutobetService {

  serverUrl: string = 'https://api.rahisibet.com/api/v1';
  errorData: {};
  redirectUrl: string;

  private autobet = new BehaviorSubject<any>(JSON.parse(localStorage.getItem("auto-betslip")));

  constructor(private httpClient: HttpClient) { }

  getAutobetGames(): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/autobet/matches`).pipe(map(data => {
      if(data['success'] != undefined && data['success'] == false ){
        let ret = [];
        ret.length = 0;
        return ret;
    }
    else {
        return data['data'];
    }
    }), catchError(this.handleError))
  }

  addAutobetSlip(betslip) {
    localStorage.setItem('auto-betslip', JSON.stringify(betslip));
    this.setAutobetSelect();
  }

  addBetslip(betslip) {
    if (localStorage.getItem("auto-betslip") === null) {
      let firstslip: any = [];
      firstslip.push(betslip);
      localStorage.setItem('auto-betslip', JSON.stringify(firstslip));
    }
    else {
      let existingbetslip = JSON.parse(localStorage.getItem("auto-betslip"));
      let betcheck = existingbetslip.find(x => x.match_id == betslip.match_id);

      if (betcheck) {
        let index = existingbetslip.indexOf(betcheck);
        if (betcheck.selected_outcome == betslip.selected_outcome) {
          existingbetslip.splice(index, 1);

          if (existingbetslip.length == 0) {
            let firstslip: any = [];
            localStorage.removeItem("auto-betslip");
          } else {
            localStorage.setItem('auto-betslip', JSON.stringify(existingbetslip));
          }
        } else {
          existingbetslip[index].selected_outcome = betslip.selected_outcome;
          existingbetslip[index].selected_market = betslip.selected_market;
          localStorage.setItem('auto-betslip', JSON.stringify(existingbetslip));
        }
      }
      else {
        if (existingbetslip.length < 14) {
          existingbetslip.push(betslip);
          localStorage.setItem('auto-betslip', JSON.stringify(existingbetslip));
        }
        else {

        }
      }
    }

    this.setAutobetSelect();
  }

  popBestlip(selectedBet) {
    let betslip_data = JSON.parse(localStorage.getItem("auto-betslip"));
    let betcheck = betslip_data.find(x => x.match_id == selectedBet.match_id);

    if (betcheck) {
      let index = betslip_data.indexOf(betcheck);
      betslip_data.splice(index, 1);

      localStorage.setItem('auto-betslip', JSON.stringify(betslip_data));
      this.setAutobetSelect();
    }

    if (betslip_data.length == 0) {
      localStorage.removeItem("auto-betslip");
    }

    this.setAutobetSelect();
  }

  clearBetslip() {
    localStorage.removeItem("auto-betslip");
    this.setAutobetSelect();
  }

  setAutobetSelect() {
    let betslip = JSON.parse(localStorage.getItem("auto-betslip"));
    this.autobet.next(betslip);
  }

  getAutobetSelect() {
    return this.autobet.asObservable();
  }

  private handleError(error: HttpErrorResponse) {
    if (error instanceof Error) {
      console.log('An error occurred:', error.error.message);
    } else {
      console.log(`Backend returned code ${error.status}, ` + `body was: ${error.error.message}`);
    }
    this.errorData = error.error.data;
    return throwError(this.errorData);
  }

}
