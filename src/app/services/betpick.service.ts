import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, Subject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BetpickService {

  constructor(private httpClient: HttpClient) { }

  private subject = new Subject<any>();
  private message = new Subject<any>();
  private betcount = new Subject<any>();
  private oddsfeed = new Subject<any>();

  serverUrl: string = 'https://api.rahisibet.com/api/v1';
  betslip: any = [];
  errorData: {};


  addBetslip(selectedBet: any) {

    let betcheck = this.betslip.find(x => x.match_id == selectedBet.match_id);

    if (betcheck) {

      let index = this.betslip.indexOf(betcheck);

      if (betcheck.selected == selectedBet.selected) {
        this.betslip.splice(index, 1);

      } else {
        this.betslip[index].selected = selectedBet.selected;
      }
    }
    else {
      if (this.betslip.length < 20) {
        let tmpBet = Object.assign({}, selectedBet);
        this.betslip.push(tmpBet);
        this.message.next(false);
      }
      else {
        this.message.next(true);
      }
    }

    this.subject.next(this.betslip);
  }

  getBetslip(): Observable<any> {
    return this.subject.asObservable();
  }

  getMessage(): Observable<any> {
    return this.message.asObservable();
  }
  setBetcount(count: number) {
    this.betcount.next(count);
  }
  getBetcount() {
    return this.betcount.asObservable();
  }
  popBetslip(selectedBet: any) {
    let betcheck = this.betslip.find(x => x.match_id == selectedBet.match_id);

    if (betcheck) {
      let index = this.betslip.indexOf(betcheck);
      this.betslip.splice(index, 1);
    }

    this.subject.next(this.betslip);
    this.message.next(false);
  }

  removeAll() {
    this.betslip.length = 0;
    this.subject.next(this.betslip);
  }

  //call create betslip api
  createBets(betslip: any): Observable<any> {

    return this.httpClient.post<any>(`${this.serverUrl}/create_betslip`, { betslip })
      .pipe(map(data => {

        if (data.success) {
          return data;
        } else {
          return data
        }
      }),
        catchError(this.handleError)
      );
  }

  getOdds(): Observable<any> {
    this.httpClient.get(`${this.serverUrl}/soccer-feeds`).subscribe(data => {
      this.oddsfeed.next(data);
    });
    return this.oddsfeed.asObservable()
  }

  private handleError(error: HttpErrorResponse) {
    if (error instanceof Error) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.

      console.log(`Backend returned code ${error.status}, ` + `body was: ${error.error.message}`);
    }

    // return an observable with a user-facing error message
    this.errorData = error.error.data;
    return throwError(this.errorData);
  }
}
