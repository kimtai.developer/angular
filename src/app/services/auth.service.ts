import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'Accept': 'application/json',
    });

    options = { headers: this.httpHeaders };
    serverUrl: string = 'https://api.rahisibet.com/api/v1';
    errorData: {};
    redirectUrl: string;

    trueFlag: Boolean = true;
    falseFlag: Boolean = false;

    private messageSource = new BehaviorSubject('');
    private balanceSubscription = new BehaviorSubject<any>(JSON.parse(localStorage.getItem("currentUser")));
    currentMessage = this.messageSource.asObservable();

    private phoneSource = new BehaviorSubject('');
    currentNumber = this.phoneSource.asObservable();

    private subject = new Subject<any>();

    constructor(private httpClient: HttpClient, ) { }

    changeMessage(message: string) { this.messageSource.next(message) }
    writePhone(phone: string) { this.phoneSource.next(phone) }

    login(phone: string, password: string) {
        return this.httpClient.post<any>(`${this.serverUrl}/login`, { "phone": phone, "password": password }, this.options)
            .pipe(map(user => {
                    if (user.success && user.data.token) {
                        localStorage.setItem('currentUser', JSON.stringify(user.data.user));
                        localStorage.setItem('token', JSON.stringify(user.data.token));
                        localStorage.setItem('expired_at', JSON.stringify(Date.parse(user.data.expires_at)));

                        this.balanceSubscription.next(JSON.parse(localStorage.getItem("currentUser")));
                        this.subject.next(this.trueFlag);

                    }else{
                        return user;
                    }
                }),
                catchError(this.handleError)
            );
    }

    register(user): Observable<any> {
        return this.httpClient.post<any>(`${this.serverUrl}/register/user`, {
                "phone": user.phone,
                "password": user.password,
                "password_confirmation": user.confirmationPassword,
                "zone":user.zone
            },
            this.options).pipe(map(data => {
                if (data.success) {
                    return true;
                }
            }),
            catchError(this.handleError)
        );
    };

    verification(phone: string, verification_code: string): Observable<any> {
        return this.httpClient.post<any>(`${this.serverUrl}/verify/otp`, { "phone": phone, "otp": verification_code }, this.options)
            .pipe(map(data => {
                    if (data.success) {
                        return data;
                    }
                    else{
                        return  data;
                    }
                }),
                catchError(this.handleError)
            );
    };

    forgotpassword(phone: string): Observable<any> {
        return this.httpClient.post<any>(`${this.serverUrl}/reset/send/otp`, { "phone": phone }, this.options)
            .pipe(map(data => {
                    if (data.success) {
                        return true;
                    }
                }),
                catchError(this.handleError)
            );
    };

    resetpassword(phone: string, password: string, password_confirmation: string, reset_code: string): Observable<any> {
        return this.httpClient.post<any>(`${this.serverUrl}/reset/password`, { "otp": reset_code, "phone": phone, "password": password, "password_confirmation": password_confirmation }, this.options)
            .pipe(map(data => {

                    if (data.success) {
                        return true;
                    }
                }),
                catchError(this.handleError)
            );
    };

    isLoggedIn() {
        if (localStorage.getItem('token') && localStorage.getItem('currentUser')) {
            return true;
        }
        else {
            return false;
        }
    }

    CheckLoggedIn(): Observable<any> {
        return this.subject.asObservable();
    }

    getAuthorizationToken() {
        const token = JSON.parse(localStorage.getItem('token'));
        return token;
    }

    getTokenExpirationDate() {
        const token = JSON.parse(localStorage.getItem('expired_at'));
        return token;
    }

    isTokenExpired(token?: string): boolean {
        if (!token) token = this.getAuthorizationToken();
        if (!token) return true;

        const date = this.getTokenExpirationDate();
        if (date === undefined) return false;
        return !(date.valueOf() > new Date().valueOf());
    }

    logout(): Observable<any> {

        return this.httpClient.post<any>(`${this.serverUrl}/logout`, {})
            .pipe(map(data => {
                    if (data.success) {
                        localStorage.removeItem('currentUser');
                        localStorage.removeItem('token');
                        localStorage.removeItem('expired_at');
                        
                        localStorage.setItem("reload-url",JSON.stringify("/"))

                        this.subject.next(this.falseFlag);
                        return true;
                    }
                }),
                catchError(this.handleError)
            );
    }

    unauthenticatedLogout(): boolean {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
        localStorage.removeItem('expired_at');
        
        this.subject.next(this.falseFlag);
        return true;
    }

    getBalance(){
        return this.balanceSubscription.asObservable();
    }
    setBalance(data){
        localStorage.setItem('currentUser', JSON.stringify(data));
        this.balanceSubscription.next(data)
    }

    private handleError(error: HttpErrorResponse) {
        if (error instanceof Error) {
            // A client-side or network error occurred. Handle it accordingly.
            console.log('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong.

            console.log(`Backend returned code ${error.status}, ` + `body was: ${error}`);
        }

        // return an observable with a user-facing error message
        this.errorData = error;
        return throwError(this.errorData);
    }

}
