import { TestBed } from '@angular/core/testing';

import { AutobetService } from './autobet.service';

describe('AutobetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutobetService = TestBed.get(AutobetService);
    expect(service).toBeTruthy();
  });
});
