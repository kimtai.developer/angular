import { TestBed } from '@angular/core/testing';

import { BetpickService } from './betpick.service';

describe('BetpickService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BetpickService = TestBed.get(BetpickService);
    expect(service).toBeTruthy();
  });
});
