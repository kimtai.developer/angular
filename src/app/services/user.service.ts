import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, throwError, BehaviorSubject, Subject } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { HttpErrorResponse } from "@angular/common/http";
import { Betslip } from "../models/betslip.model";

@Injectable({
  providedIn: "root",
})
export class UserService {
  serverUrl: string = "https://api.rahisibet.com/api/v1";
  errorData: {};
  redirectUrl: string;

  private subjectBetslip = new BehaviorSubject<any>(
    JSON.parse(localStorage.getItem("betslip"))
  );
  private betcount = new BehaviorSubject<any>(0);
  private message = new Subject<any>();
  private subjectSlips = new BehaviorSubject<any>([]);
  private metadata = new BehaviorSubject<any>([]);
  constructor(private httpClient: HttpClient) {}

  addBetslip(betslip) {
    if (localStorage.getItem("betslip") === null) {
      let firstslip: any = [];
      firstslip.push(betslip);
      localStorage.setItem("betslip", JSON.stringify(firstslip));
    } else {
      let existingbetslip = JSON.parse(localStorage.getItem("betslip"));
      let betcheck = existingbetslip.find((x) => x.id == betslip.id);

      if (betcheck) {
        let index = existingbetslip.indexOf(betcheck);
        if (betcheck.selected_outcome == betslip.selected_outcome) {
          existingbetslip.splice(index, 1);

          if (existingbetslip.length == 0) {
            let firstslip: any = [];
            localStorage.removeItem("betslip");
          } else {
            localStorage.setItem("betslip", JSON.stringify(existingbetslip));
          }
        } else {
          existingbetslip[index].selected_odd_id = betslip.selected_odd_id;
          existingbetslip[index].selected_outcome = betslip.selected_outcome;
          existingbetslip[index].selected_market = betslip.selected_market;
          localStorage.setItem("betslip", JSON.stringify(existingbetslip));
        }
      } else {
        if (existingbetslip.length < 20) {
          existingbetslip.push(betslip);
          localStorage.setItem("betslip", JSON.stringify(existingbetslip));
          this.message.next(false);
        } else {
          this.message.next(true);
        }
      }
    }
    let betslip_data = JSON.parse(localStorage.getItem("betslip"));
    this.subjectBetslip.next(betslip_data);
    this.setBetcount(betslip_data == null ? 0 : betslip_data.length);
  }

  onOddsChange(data) {
    let existingbetslip = JSON.parse(localStorage.getItem("betslip"));
    data.forEach((el) => {
      let betcheck = existingbetslip.find((x) => x.id == el.pick);
      if (betcheck) {
        let index = existingbetslip.indexOf(betcheck);
        let market =
          existingbetslip[index].markets[
            existingbetslip[index].selected_market
          ];
        market.forEach((elem) => {
          if (elem.sr_outcome_id == el.outcome) {
            elem.odds = el.odds;
          }
        });
      }
    });

    localStorage.setItem("betslip", JSON.stringify(existingbetslip));

    let betslip_data = JSON.parse(localStorage.getItem("betslip"));
    this.subjectBetslip.next(betslip_data);
  }

  onBetstop(data) {
    let existingbetslip = JSON.parse(localStorage.getItem("betslip"));
    data.forEach((el) => {
      let betcheck = existingbetslip.find((x) => x.id == el);
      if (betcheck) {
        let index = existingbetslip.indexOf(betcheck);
        existingbetslip.splice(index, 1);

        localStorage.setItem("betslip", JSON.stringify(existingbetslip));
      }
    });

    let r_betslip_data = JSON.parse(localStorage.getItem("betslip"));
    this.subjectBetslip.next(r_betslip_data);
    this.setBetcount(r_betslip_data == null ? 0 : r_betslip_data.length);
  }

  setSubjectBetslip() {
    if (localStorage.getItem("betslip") !== null) {
      let betslip_data = JSON.parse(localStorage.getItem("betslip"));
      this.subjectBetslip.next(betslip_data);
    } else {
      this.subjectBetslip.next(null);
    }
  }

  getBetslipSubject() {
    return this.subjectBetslip.asObservable();
  }

  popBestlip(selectedBet) {
    console.log("sele", selectedBet);

    let betslip_data = JSON.parse(localStorage.getItem("betslip"));
    let betcheck = betslip_data.find((x) => x.id == selectedBet.id);

    if (betcheck) {
      let index = betslip_data.indexOf(betcheck);
      betslip_data.splice(index, 1);

      localStorage.setItem("betslip", JSON.stringify(betslip_data));
      this.message.next(false);
    }

    if (betslip_data.length == 0) {
      localStorage.removeItem("betslip");
    }

    let r_betslip_data = JSON.parse(localStorage.getItem("betslip"));
    this.subjectBetslip.next(r_betslip_data);
    this.setBetcount(r_betslip_data == null ? 0 : r_betslip_data.length);
  }

  clearBetslip() {
    localStorage.removeItem("betslip");
    let r_betslip_data = JSON.parse(localStorage.getItem("betslip"));
    this.subjectBetslip.next(r_betslip_data);
    this.setBetcount(0);
  }

  getMessage(): Observable<any> {
    return this.message.asObservable();
  }

  setBetcount(count: number) {
    this.betcount.next(count);
  }

  getBetcount() {
    return this.betcount.asObservable();
  }
  // betslip
  sendBetslip(betslip): Observable<any> {
    return this.httpClient
      .post<Betslip>(`${this.serverUrl}/place_bet`, {
        amount: betslip.amount,
        bettype: betslip.bettype,
        picks: betslip.picks,
      })
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.handleError)
      );
  }

  // autobet
  sendAutoBetslip(betslip): Observable<any> {
    return this.httpClient
      .post<Betslip>(`${this.serverUrl}/place/auto_bet`, {
        amount: betslip.amount,
        bettype: betslip.bettype,
        picks: betslip.picks,
      })
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.handleError)
      );
  }

  //jackpot
  sendJackpotBetslip(betslip): Observable<any> {
    return this.httpClient
      .post<Betslip>(`${this.serverUrl}/place/jackpot`, {
        amount: betslip.amount,
        bettype: betslip.bettype,
        picks: betslip.picks,
      })
      .pipe(
        map((data) => {
          return data;
        }),
        catchError(this.handleError)
      );
  }

  inviteFriend(friend): Observable<any> {
    return this.httpClient
      .post<any>(`${this.serverUrl}/invite/`, { phone: friend.friend })
      .pipe(
        map((data) => {
          if (data.success) {
            return true;
          } else {
            return false;
          }
        }),
        catchError(this.handleError)
      );
  }

  updateProfile(profile): Observable<any> {
    return this.httpClient
      .post<any>(`${this.serverUrl}/update/user`, {
        name: profile.name,
        email: profile.email,
        town: profile.town,
        surname: profile.surname,
        gender: profile.gender,
        zone_id: profile.zone,
      })
      .pipe(
        map((data) => {
          if (data.success) {
            localStorage.setItem("currentUser", JSON.stringify(data.data.user));
            return data;
          } else {
          }
          return data;
        }),
        catchError(this.handleError)
      );
  }

  updatePassword(password_obj): Observable<any> {
    return this.httpClient
      .post<any>(`${this.serverUrl}/update/password/profile`, {
        password: password_obj.password,
      })
      .pipe(
        map((data) => {
          if (data.success) {
            return data;
          } else {
          }
        }),
        catchError(this.handleError)
      );
  }

  //get betslip
  getBetslip(): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/user/bets`).pipe(
      map((data) => {
        this.subjectSlips.next(data["data"]);
        this.metadata.next(data["meta"]);
      }),
      catchError(this.handleError)
    );
  }

  getSlipsByDateRange(from, to): Observable<any> {
    return this.httpClient
      .post<any>(`${this.serverUrl}/user/search/bets`, { from: from, to: to })
      .pipe(
        map((data) => {
          this.subjectSlips.next(data["data"]);
          this.metadata.next(data["meta"]);
        }),
        catchError(this.handleError)
      );
  }

  getSingleSlip(id): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/user/bets/` + id).pipe(
      map((data) => {
        this.subjectSlips.next(data["data"]);
        this.metadata.next(data["meta"]);
      }),
      catchError(this.handleError)
    );
  }

  getSlips(): Observable<any> {
    return this.subjectSlips.asObservable();
  }

  getMetadata(): Observable<any> {
    return this.metadata.asObservable();
  }

  // pagination
  getNextPage(page, path): Observable<any> {
    return this.httpClient.get(path + "?page=" + page, {}).pipe(
      map((data) => {
        this.subjectSlips.next(data["data"]);
      }),
      catchError(this.handleError)
    );
  }

  getNextSlipPage(page, path): Observable<any> {
    return this.httpClient.get(path + "?page=" + page, {}).pipe(
      map((data) => {
        this.subjectSlips.next(data["data"]);
      }),
      catchError(this.handleError)
    );
  }

  getNextByDateRange(page, path, from, to): Observable<any> {
    return this.httpClient
      .post<any>(path + "?page=" + page, { from: from, to: to })
      .pipe(
        map((data) => {
          this.subjectSlips.next(data["data"]);
        }),
        catchError(this.handleError)
      );
  }
  getUser(user_id): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/user/details`).pipe(
      map((data) => {
        if (data["success"]) {
          return data["data"][0];
        }
      }),
      catchError(this.handleError)
    );
  }

  getZones(): Observable<any> {
    return this.httpClient.get(`${this.serverUrl}/zones`).pipe(
      map((data) => {
        if (data["success"]) {
          return data["data"];
        }
      }),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error instanceof Error) {
      console.log("An error occurred:", error.message);
    } else {
      console.log(
        `Backend returned code ${error.status}, ` + `body was: ${error.message}`
      );
    }
    this.errorData = error.message;
    return throwError(this.errorData);
  }
}
