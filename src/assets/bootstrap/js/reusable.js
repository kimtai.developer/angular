<!-- Resusable widget -->
<div class="reusable-sport" *ngIf="oddsfeed.length > 0">
    <div class="reusable-heading hidden-xs">
        <div>
            <h4 class="sport-header">{{ 'feeds.featured-sports' | translate }}</h4>
        </div>
        <ng-container *ngIf="!today_games">
            <!-- empty div for single market types -->
            <div class="hidden-xs text-center" *ngIf="markets.market.length == 1"
                [ngClass]="{ 'half-width' : !today_games }">
            </div>
            <ng-container *ngFor="let market of markets.market">
                <div class="hidden-xs text-center" *ngIf="markets.marketoffered.indexOf(market) > -1">
                    <span class="market-header top">{{ market }}</span>
                </div>
            </ng-container>
        </ng-container>
    </div>
    <div class="reusable-match">
        <ul class="list-unstyled border-top">
            <li *ngFor="let game of oddsfeed; let i=index">
                <div class="match-details">
                    <div class="match-description">
                        <div class="time-gameid-details"><span><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;
                                {{ game.scheduled }} </span>&nbsp;&nbsp;<span>{{ 'feeds.game-id' | translate }}:
                                {{game.game_id}}</span>
                        </div>

                        <div class="league-teams" *ngIf="translate.currentLang == 'en'">{{ game.name }}</div>
                        <div class="league-teams" *ngIf="translate.currentLang == 'fr'">{{ game.french }}</div>

                        <div class="league-details" *ngIf="translate.currentLang == 'en'">{{game.sport}} -
                            {{game.country}} - {{game.tournament}}</div>
                        <div class="league-details" *ngIf="translate.currentLang == 'fr'">{{game.sport_fr_name}} -
                            {{game.country_fr_name}} - {{game.tournament_fr_name}}</div>


                    </div>
                    <ng-container *ngFor="let market of markets.market">
                        <ng-container
                            *ngIf="markets.marketoffered.indexOf(market) > -1 && game_markets[i].indexOf(market) > -1"
                            [ngSwitch]="markets.markettypes[market]">

                            <!-- add empty div -->
                            <div class="market-container text-center hidden-xs" *ngIf="game_markets[i].length == 1"
                                [ngClass]="{ 'half-width' : !today_games }">
                            </div>

                            <div class="market-container text-center" *ngSwitchCase="2">
                                <ng-container
                                    *ngIf="game.markets[market] != undefined && game.markets[market].length == 2">
                                    <div class="market-container text-center">
                                        <div class="moblie-market-header visible-xs">{{ market }}</div>
                                        <div class="moblie-market-header hidden-xs" *ngIf="today_games">{{ market }}
                                        </div>
                                        <div class="market-header">
                                            <div><span class="market-type">1</span></div>
                                            <div><span class="market-type">2</span></div>
                                        </div>
                                        <div class="market-odds">
                                            <div *ngFor="let odd_values of game.markets[market]; let y=index">
                                                <button type="button" name="button" class="btn btn-market"
                                                    *ngIf="odd_values.sr_outcome_id"
                                                    (click)="addToBetslip(odd_values.sr_outcome_id,game.match_id,odd_values.market_name)"
                                                    [ngClass]="{ 'selected' : game.selected_outcome == odd_values.sr_outcome_id }">
                                                    {{ odd_values.odds | number:'1.2-2' }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </ng-container>
                            </div>

                            <div class="market-container text-center" *ngSwitchCase="3">
                                <ng-container
                                    *ngIf="game.markets[market] != undefined && game.markets[market].length > 2">
                                    <div class="market-container text-center"
                                        *ngIf="market == markets.marketoffered[1] && game_markets[i].indexOf(market) > -1">
                                        <div class="moblie-market-header visible-xs">{{ market }}</div>
                                        <div class="moblie-market-header hidden-xs" *ngIf="today_games">{{ market }}
                                        </div>
                                        <div class="market-header">
                                            <div><span class="market-type">1 {{ 'feeds.or' | translate }} X</span></div>
                                            <div><span class="market-type">1 {{ 'feeds.or' | translate }} 2</span></div>
                                            <div><span class="market-type">X {{ 'feeds.or' | translate }} 2</span></div>
                                        </div>
                                        <div class="market-odds">
                                            <div *ngFor="let odd_values of game.markets[market]; let x=index">
                                                <button type="button" name="button" class="btn btn-market"
                                                    *ngIf="odd_values.sr_outcome_id"
                                                    (click)="addToBetslip(odd_values.sr_outcome_id,game.match_id,odd_values.market_name)"
                                                    [ngClass]="{ 'selected' : game.selected_outcome == odd_values.sr_outcome_id }">
                                                    {{ odd_values.odds | number:'1.2-2' }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="market-container text-center"
                                        *ngIf="market != markets.marketoffered[1] && game_markets[i].indexOf(market) > -1">
                                        <div class="moblie-market-header visible-xs">{{ market }}</div>
                                        <div class="moblie-market-header hidden-xs" *ngIf="today_games">{{ market }}
                                        </div>
                                        <div class="market-header">
                                            <div><span class="market-type">1</span></div>
                                            <div><span class="market-type">X</span></div>
                                            <div><span class="market-type">2</span></div>
                                        </div>
                                        <div class="market-odds">
                                            <div *ngFor="let odd_values of game.markets[market]; let y=index">
                                                <button type="button" name="button" class="btn btn-market"
                                                    *ngIf="odd_values.sr_outcome_id"
                                                    (click)="addToBetslip(odd_values.sr_outcome_id,game.match_id,odd_values.market_name)"
                                                    [ngClass]="{ 'selected' : game.selected_outcome == odd_values.sr_outcome_id }">
                                                    {{ odd_values.odds | number:'1.2-2' }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </ng-container>
                            </div>
                        </ng-container>
                    </ng-container>
                </div>
            </li>
        </ul>
    </div>
</div>
